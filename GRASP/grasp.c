#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <gsl/gsl_rng.h>

double euclid(int n, double vector_1[], double vector_2[])
{
/*! calculate the Euclidean distance between the 'vector_1' anche the 'vector_2', of dimensions 'n' */

      int j; 
      double s, d0, d;

      s = 0; 
      for (j=0; j<n; j++)
      {
          d0 = (vector_1[j] - vector_2[j]); 
          s = s + pow(d0,2);
      }   
      d = sqrt(s);
      return d;
}

int rand_int(int n_min,int n_max,gsl_rng *set_rng)
{
/*! generate a random integer number included in the range [n_min,n_max] */

    int r;
    r = round((n_max - n_min)*gsl_rng_uniform(set_rng) + n_min);    
    return r;
}

void rand_int_v(int *v,int *v_basin,int n,int m,gsl_rng *set_rng)
{
/*! it extracts randomly n elements from the vector 'v_basin' (of size 'm') and it puts them in the vector 'v', without repetition */
  
  int i,j,c,ind;

  ind = rand_int(0,m,set_rng);
  v[0] = ind;

  for (i=1; i<n; i++)
  {
      c = 0;
      while (c == 0)  
      {           
           c = 1;
           ind = rand_int(0,m,set_rng); 
           for (j=0; j<i; j++)
           {
               if (ind == v[j])
               { 
                   c = 0;
                   break;
               }
           }
      }
      v[i] = ind; 
  }    
}
 
void sort(double *vector,size_t rows)
{
/*! sort by ascending values the elements of a vector 'vector' of size 'rows' */

        size_t i, j;
        double temp_vector;

        for (i=0; i<rows; i++)
        {
            for (j=i; j<rows; j++)
            {
                if(vector[i] > vector[j])
                {
                   temp_vector = vector[i];
                   vector[i] = vector[j];
                   vector[j] = temp_vector;  
                }
            }
        }     
}
 
void sort_ind(double *d,int *ind,size_t rows)
{
/*! sort a vector 'd' of size 'rows'*/

        size_t i, j;
        double d_temp;
        int ind_t;

        for (i=0; i<rows; i++)
             ind[i] = i;

        for (i=0; i<rows; i++)
        {
            for (j=i; j<rows; j++)
            {
                if(d[i] > d[j])
                {
                   d_temp = d[i];
                   d[i] = d[j];
                   d[j] = d_temp;
                   ind_t = ind[i];
                   ind[i] = ind[j];
                   ind[j] = ind_t;    
                }
            }
        }     
}

void row_from_mat(double *v,double *mat,int n, size_t m)
{
/*! This function puts the n-th rows of the matrix mat in the vector v
@param v vector 
@param mat matrix
@param n index of the row to extract
@param m number of the columns of the matrix */
    
   size_t j;

   for (j=0; j<m; j++)
            v[j] = mat[n*m+j];
}

void row_from_mat_int(int *v,int *mat,int n, size_t m)
{
/*! This function puts the n-th rows of the matrix mat in the vector v
@param v vector 
@param mat matrix
@param n index of the row to extract
@param m number of the columns of the matrix */
    
   size_t j;

   for (j=0; j<m; j++)
            v[j] = mat[n*m+j];
}

void col_from_mat(double *v,double *mat,int n,int nrow,int ncol)
{
/*! This function puts the n-th column of the matrix mat in the vector v
@param v vector 
@param mat matrix
@param n index of the column to extract
@param nrow number of the rows of the matrix
@param ncol number of the columns of the matrix */
    
   int j;

   for (j=0; j<nrow; j++)
            v[j] = mat[ncol*j+n];
}

void mat_from_ind(double *M_tot,double *M_red,int *ind,int n,int m,int d)
{
/*! extract the 'ind' rows from the M_tot matrix to generate a reduced matrix M_red 
@param n number of rows of the M_tot matrix
@param m number of columns of the M_tot and M_red matrices
@param d size of the ind vector and number of the rows of the M_red matrix */

   int i,j;
   double v_ext[m];

   for (i=0; i<d; i++)
   {
       row_from_mat(&v_ext[0],&M_tot[0],ind[i],m);
       for (j=0; j<m; j++)
       {
           M_red[i*m+j] = v_ext[j];  
       } 
   } 
}

int n_in_vec(int *vec,int d,int n)
{
/*! it counts the occurences of the integer 'n' in the vector 'vec' of size 'd' */ 

    int nv,i;
    int *temp;

    nv = 0;
    for (i=0; i<d; i++)
    {
        temp = &vec[i];
        if (n == *temp)
            nv = nv + 1;  
    }     
    return nv; 
}

double ds_score(int n, int m,double *k_test,int M)
{ 
/*! This function calculates the dissimilarity score.
@param n number of the vectors of the set
@param m dimensions of the vectors
@param k_test matrix n x m of the vectors
@param M number of the nearest vectors included in the ds score */

       double ds, d[n][n], d_sort[n][n];
       double v_ref[m],v_test[m],v_sort[n];   
       int i,j, g[M];         

       ds = 0;

       for (i=0; i<n; i++)
       {
           row_from_mat(&v_ref[0],&k_test[0],i,m);
           for (j=0; j<n; j++)
           {
               row_from_mat(&v_test[0],&k_test[0],j,m);  
               d[i][j] = euclid(m,v_ref,v_test);                
           } 
       }      

       for (i=0; i<n; i++)
       {
           for (j=0; j<n; j++)
           {
               d_sort[i][j] = d[i][j]; 
           }
       }        
      
       for (i=0; i<n; i++)
       { 
           col_from_mat(&v_sort[0],&d[0][0],i,n,n);
           sort(&v_sort[0],n); 
           for (j=0; j<n; j++)
           {
               d_sort[j][i] = v_sort[j];  
           } 
       }        

       for (i=0; i<M; i++) 
       { 
           g[i] = pow(2,M-i-1); 
       }

       for (i=0; i<n; i++)
       {
           for(j=0; j<M; j++) 
           {
              ds = ds + d_sort[j+1][i]*g[j]/(n*M); 
           }
       }       
       return ds; 
}


int bisample(double b,gsl_rng *set_rng)
{
/*! the bisample function extracts an integer between 1 to 64 according to two different probability distributions (pdf) depending from the value of the input variable 'b'
    both the pdf are exponential, giving larger proability for the selection of lower numbers than higher, however, if b < 0.5 the pdf is more uniform */  

    int i,p,n;
    double range[7];
    double a;

    if (b<0.5)
    {
    range[0] = 0.1; 
    range[1] = 0.2;
    range[2] = 0.4;
    range[3] = 0.6;
    range[4] = 0.8;
    range[5] = 0.9; 
    range[6] = 1;
    }
    else
    {
    range[0] = 0.2; 
    range[1] = 0.4;
    range[2] = 0.8;
    range[3] = 0.9;
    range[4] = 0.95;
    range[5] = 0.995; 
    range[6] = 1;
    }  

    a = gsl_rng_uniform(set_rng);
    for (i=0; i<7; i++)
    {
        if(i==1)
        {
          if(a <= range[i])
             n=1;
        } 
        else
        {
          if(a > range[i-1] && a <= range[i])
             {
             p = pow(2,i); 
             n = rand_int(1,p,set_rng);
             }
        }  
    }
    return n; 
}

void initialize_c(int *i_trial,double *c,int n,int m,int M,gsl_rng *set_rng,double r_basin_ini)
{
/*! initialize_c generates the initial set of M vectors in the construction phase of the GRASP algorithm
@param c the matrix of the candidate vectors
@param i_trial the partial solution of M vectors */   

   int N_max = round(n*r_basin_ini);
   int i,j,M_temp,i_sel;
   int i_tot[n],i_j[N_max],ind[N_max];
   double c_red[M][m],i_test[M],ds_store[N_max];
   double b;   

   for (i=0; i<n; i++)          /*! definition of the basin vector */  
        i_tot[i] = i;

   rand_int_v(&i_trial[0],&i_tot[0],M,n-1,set_rng);
   for (i=0; i<M; i++)
   {
        i_test[i] = i_trial[i];
    } 
   mat_from_ind(&c[0],&c_red[0][0],&i_trial[0],n,m,6);

   for (i=1; i<M; i++)
   {
       M_temp = i+1;  
       for (j=0; j<N_max; j++) 
       {
           i_j[j] = rand_int(0,n,set_rng);
           i_trial[i] = i_j[j];
           mat_from_ind(&c[0],&c_red[0][0],&i_trial[0],n,m,i+1);
           ds_store[j] = ds_score(i+1,m,&c_red[0][0],M_temp);              
       }

       sort_ind(&ds_store[0],&ind[0],N_max); 
       b = gsl_rng_uniform(set_rng);        
       i_sel = bisample(b,set_rng);
       i_trial[i] = i_j[ind[N_max-i_sel]]; 
       printf("%4i %6.4f %4i\n",i+1, ds_store[N_max-i_sel],i_sel);

   }

}

void gen_cand_costruction(int *i_j,int *i_tot,int *i_trial,int d,int N_max,int n,gsl_rng *set_rng)
{
/*! This function exctracts randomly 'N_max' elements from the vector 'i_tot' (of size 'n') 
   without repetition and with all the elements different of the first 'd' of the vector i_trial.
   The result is put in the vector 'i_j' */ 

  int i,j,c,ind,nv;

  for (i=0; i<N_max; i++)
  {
      c = 0;
      while (c == 0)  
      {           
           c = 1;
           ind = rand_int(0,n-1,set_rng); 
           for (j=0; j<i; j++)
           {
               if (ind == i_j[j])
               { 
                   c = 0;
                   break;
               }
           }
           nv = n_in_vec(&i_trial[0],d,ind);
           if (nv != 0)
              c = 0;
      }
      i_j[i] = i_tot[ind];
  } 
}

double construction(int *i_trial,double *c,int n,int m,int M,int n_tr,gsl_rng *set_rng,double r_basin)
{
/*! This function performs the construction phase of the GRASP algorithm; the function returs the value of the dissimilarity score of the solution proposed
@param i_trial the solution generated by the construction phase 
@param c the matrix of the candidate vectors
@param n size of the set of the candidate vectors
@param m dimension of the vectors
@param M parameter of the dissimilarity score, it defines the number of the nearest vectors considered
@param n_tr number of the vectors included in the solution */

   int N_max = round(n*r_basin);
   int i,j,i_sel,k,b;
   double ds, ds_temp;
   int i_tot[n],i_j[N_max],ind[N_max];
   double c_red[n_tr][m],ds_store[N_max];

   for (i=0; i<n; i++)          /*! definition of the basin vector */        
       i_tot[i] = i;        
 
   for (i=M; i<n_tr; i++)
   { 
       gen_cand_costruction(&i_j[0],&i_tot[0],&i_trial[0],i,N_max,n,set_rng);
       for (j=0; j<N_max; j++) 
       {     
           i_trial[i] = i_j[j];
           mat_from_ind(&c[0],&c_red[0][0],&i_trial[0],n,m,i+1);
           ds_store[j] = ds_score(i+1,m,&c_red[0][0],M);
       } 
       sort_ind(&ds_store[0],&ind[0],N_max);
       b = gsl_rng_uniform(set_rng);
       i_sel = bisample(b,set_rng);
       i_trial[i] = i_j[ind[N_max-i_sel]];       
       printf("%4i  %6.4f %4i %4i\n",i+1, ds_store[N_max-i_sel],i_sel,i_j[ind[N_max-i_sel]]);    
   }
   ds = ds_store[N_max-i_sel-1];
   return ds;
}

void concatenate_mat(double *c_red,double *c_red_new,int m,int n_old,int n_add)
{
   int i,j;
   for (i=0; i<n_add; i++)
   {
       for (j=0; j<m; j++)
            c_red[(i+n_old)*m+j] = c_red_new[i*m+j];
   } 
}

double construction_cum(int *i_trial,double *c_red,double *c,int n,int m,int M,int n_tr_tot,int n_tr_sel,gsl_rng *set_rng,double r_basin)
{
/*! This function performs the construction phase of the GRASP algorithm; the function returs the value of the dissimilarity score of the solution proposed
@param i_trial the solution generated by the construction phase 
@param c the matrix of the candidate vectors
@param n size of the set of the candidate vectors
@param m dimension of the vectors
@param M parameter of the dissimilarity score, it defines the number of the nearest vectors considered
@param n_tr number of the vectors included in the solution */

   int N_max = round(n*r_basin);
   int i,j,i_sel,k,b,n_old_sol;
   double ds, ds_temp;
   int i_tot[n],i_j[N_max],ind[N_max];
   double ds_store[N_max],c_red_new[n_tr_sel][m];

   n_old_sol = n_tr_tot-n_tr_sel;

   for (i=0; i<n; i++)          /*! definition of the basin vector */        
       i_tot[i] = i;        
 
   for (i=0; i<n_tr_sel; i++)
   { 
       gen_cand_costruction(&i_j[0],&i_tot[0],&i_trial[0],i,N_max,n,set_rng);
       for (j=0; j<N_max; j++) 
       {     
           i_trial[i] = i_j[j];
           mat_from_ind(&c[0],&c_red_new[0][0],&i_trial[0],n,m,i+1);
           concatenate_mat(&c_red[0],&c_red_new[0][0],m,n_old_sol,i+1); 
           ds_store[j] = ds_score(n_old_sol+i+1,m,&c_red[0],M);
       } 
       sort_ind(&ds_store[0],&ind[0],N_max);
       b = gsl_rng_uniform(set_rng);
       i_sel = bisample(b,set_rng);
       i_trial[i] = i_j[ind[N_max-i_sel]];  
       mat_from_ind(&c[0],&c_red_new[0][0],&i_trial[0],n,m,i+1);
       concatenate_mat(&c_red[0],&c_red_new[0][0],m,n_old_sol,i+1);     
       printf("%4i  %6.4f %4i %4i\n",i+1, ds_store[N_max-i_sel],i_sel,i_j[ind[N_max-i_sel]]);   
/*       for (j=0; j<i+1; j++)
            printf("%6i  %6.4f  %6.4f \n",i_trial[j],c_red_new[j][0],c_red_new[j][1]); 
       for (j=0; j<n_old_sol+i+1; j++)
            printf("c_red %6.4f  %6.4f \n",c_red[2*j],c_red[2*j+1]); */
   }
   ds = ds_store[N_max-i_sel-1];
   return ds;
}

int gen_i_new(int *i_trial,int n_tr,int *i_tot,int n,gsl_rng *set_rng)
{
/*! gen_i_new selects randomly a new vector not already included in the trial solution to build a new solution in the local search of GRASP */

    int N_max,cont,c,nv,i,i_new;

    N_max = 2000;
    cont = 0;
    c = 0;
    while (c == 0)
    {
        c = 1;
        cont++;
        i = rand_int(0,n,set_rng); 
        i_new = i_tot[i];
        nv = n_in_vec(&i_trial[0],n_tr,i_new);
        if (nv > 0)
           c = 0;
        if (cont > N_max)
        {
           printf("Error: maximum number of random generations exceeded in gen_i_new function \n");
           exit(0);
        }   
    }
    return i_new;
}

void local_search_scrambled(int *i_trial,double *c,int n,int m,int M,int n_tr,gsl_rng *set_rng, int i_power,int c_max)
{
/*! stochastic local search  */

   int i,j,i_change,i_cont,i_old,i_new,N_sample,nv;
   double ds, ds_temp;
   int i_tot[n],i_red[n],rand_ord[n_tr];
   double c_red[n_tr][m];

   int i_p;  

   i_change = 1;
   i_cont = 0; 

   for (i=0; i<n; i++)          /*! definition of the basin vector */        
       i_tot[i] = i;

   for (i=0; i<n_tr; i++)          /*! initialization of the order vector */        
       rand_ord[i] = i;
        
   mat_from_ind(&c[0],&c_red[0][0],&i_trial[0],n,m,n_tr);
   ds = ds_score(n_tr,m,&c_red[0][0],M);
 
   while (i_change == 1)
   {
       i_change = 0;
       i_cont = i_cont + 1;
 
       printf("Outer Cycle Number %4i \n",i_cont); 
       N_sample = pow(2,i_power);
       if (N_sample > n)
           N_sample = n;
       for (i=0; i<N_sample; i++)
       {
           printf("%4i \n", i);
           rand_int_v(&rand_ord[0],&rand_ord[0],n_tr,n_tr,set_rng);
           for (j=0; j<n_tr; j++)
           {
               i_old = i_trial[rand_ord[j]];               
               i_new = gen_i_new(&i_trial[0],n_tr,&i_tot[0],n,set_rng);                
               i_trial[rand_ord[j]] = i_new;
               mat_from_ind(&c[0],&c_red[0][0],&i_trial[0],n,m,n_tr);
               ds_temp = ds_score(n_tr,m,&c_red[0][0],M);  
               if (ds_temp > ds)
               {
                  ds = ds_temp;
                  i_trial[rand_ord[j]] = i_new;
                  i_change = 1;
                  printf("%4i %6.4f \n",j,ds);   
               } 
               else
                  i_trial[rand_ord[j]] = i_old;                     
           }
       }
       i_power = i_power - 1;
       if (i_power == 0)  
           i_power = 1;  
       if (i_cont == c_max)  
           i_change = 0;
   }
   return;
}

double local_search_scrambled_cum(int *i_trial,double *c_red,double *c,int n,int m,int M,int n_tr_tot,int n_tr_sel,gsl_rng *set_rng, int i_power,int c_max)
{
/*! stochastic local search for the cumulative optimization */

   int i,j,i_change,i_cont,i_old,i_new,N_sample,nv,n_tr_old;
   double ds, ds_temp;
   int i_tot[n],i_red[n],rand_ord[n_tr_sel];

   n_tr_old = n_tr_tot - n_tr_sel;

   double c_red_old[n_tr_old][m],c_red_new[n_tr_sel][m];
   int i_red_old[n_tr_old],i_red_new[n_tr_sel];
   int i1;

   i_change = 1;
   i_cont = 0; 

   for (i=0; i<n; i++)          /*! definition of the basin vector */        
       i_tot[i] = i;

   for (i=n_tr_old; i<n_tr_tot; i++)          /*! initialization of the order vector */        
       rand_ord[i] = i;
        
   ds = ds_score(n_tr_tot,m,&c_red[0],M);

/*   for (i=0; i<n_tr_old; i++)
        i_red_old[i]=i; */

   for (i=0; i<n_tr_sel; i++)
        i_red_new[i]=i+n_tr_old;      

/*   mat_from_ind(&c_red[0],&c_red_old[0][0],&i_red_old[0],n,m,n_tr_old); */
   mat_from_ind(&c_red[0],&c_red_new[0][0],&i_red_new[0],n,m,n_tr_sel);

   while (i_change == 1)
   {
       i_change = 0;
       i_cont = i_cont + 1;
 
       printf("Outer Cycle Number %4i \n",i_cont); 
       N_sample = pow(2,i_power);
       if (N_sample > n)
           N_sample = n;
       for (i=0; i<N_sample; i++)
       {
           printf("%4i \n", i);
           rand_int_v(&rand_ord[0],&rand_ord[0],n_tr_sel,n_tr_sel,set_rng);
           for (j=0; j<n_tr_sel; j++)
           {
               i_old = i_trial[rand_ord[j]];               
               i_new = gen_i_new(&i_trial[0],n_tr_tot,&i_tot[0],n,set_rng);                
               i_trial[rand_ord[j]] = i_new;
               mat_from_ind(&c[0],&c_red_new[0][0],&i_trial[0],n,m,n_tr_sel);
               concatenate_mat(&c_red[0],&c_red_new[0][0],m,n_tr_old,n_tr_sel);
               ds_temp = ds_score(n_tr_tot,m,&c_red[0],M);  
               if (ds_temp > ds)
               {
                  ds = ds_temp;
                  i_trial[rand_ord[j]] = i_new;
                  i_change = 1;
                  printf("%4i %6.4f \n",j,ds);   
               } 
               else
                  i_trial[rand_ord[j]] = i_old;                     
           }
       }
       i_power = i_power - 1;
       if (i_power == 0)  
           i_power = 1;  
       if (i_cont == c_max)  
           i_change = 0;
   }
   for (i=0; i<n_tr_sel; i++)
        printf("%4d ",i_trial[i]);

   printf("%8.4f \n",ds);

   return ds;
}

void local_search_loc(int *i_trial,double *c,int n,int m,int M,int n_tr,int i_power,int c_max)
{
/*! local search that optimizes the trial solution replacing the vectors with the nearest vectors to them */

   int i,j,i_change,i_cont,i_max,N_sample,nv;
   double ds, ds_temp;
   int ind[n],i_red[n];
   double c_red[n_tr][m];
   double v_ref[m],v_test[m],d[n];

   int i_p;

   mat_from_ind(&c[0],&c_red[0][0],&i_trial[0],n,m,n_tr);
   ds = ds_score(n_tr,m,&c_red[0][0],M); 

   i_change = 1;
   i_cont = 0; 

   for (i=0; i<n; i++)          /*! definition of the index vector */        
       ind[i] = i;
         
   printf("Neighborhood Local Search \n"); 
   while (i_change == 1)
   {
       i_change = 0;
       i_cont = i_cont + 1;
       for (i=0; i<n_tr; i++)
       {
           printf("%4i \n",i); 
           N_sample = pow(2,i_power);
           if (N_sample > n)
               N_sample = n;

           i_max = i_trial[i];
           row_from_mat(&v_ref[0],&c[0],i_max,m);
           for (j=0; j<n; j++)
           {
               row_from_mat(&v_test[0],&c[0],j,m);  
               d[j] = euclid(m,v_ref,v_test);                
           }             
           sort_ind(&d[0],&ind[0],n);
           for (j=0; j<N_sample; j++)
                i_red[j] = ind[j+1];  

           for (j=0; j<N_sample; j++)
           {
               nv = n_in_vec(&i_trial[0],n_tr,i_red[j]);
               if (nv > 0)
                   continue;
               
               i_trial[i] = i_red[j];
               mat_from_ind(&c[0],&c_red[0][0],&i_trial[0],n,m,n_tr);
               ds_temp = ds_score(n_tr,m,&c_red[0][0],M);  
               if (ds_temp > ds)
               {
                  ds = ds_temp;
                  i_max = i_trial[i];
                  i_change = 1;
                 printf("%4i %6.4f \n",j,ds);   
               }     
           }
           i_trial[i] = i_max;
       }
       i_power = i_power - 1;
       if (i_power == 0)  
           i_power = 1;  
       if (i_cont == c_max)  
           i_change = 0;
   }
   return;
}

double local_search_loc_cum(int *i_trial,double *c_red,double *c,int n,int m,int M,int n_tr_tot,int n_tr_sel,int i_power,int c_max)
{
/*! local search that optimizes the trial solution replacing the vectors with the nearest vectors to them */

   int i,j,i_change,i_cont,i_max,N_sample,nv,n_tr_old;
   double ds, ds_temp;
   int ind[n],i_red[n];
   double v_ref[m],v_test[m],d[n];

   n_tr_old = n_tr_tot - n_tr_sel;

   double c_red_old[n_tr_old][m],c_red_new[n_tr_sel][m];
   int i_red_old[n_tr_old],i_red_new[n_tr_sel];
    
   for (i=0; i<n_tr_old; i++)
        i_red_old[i]=i;

   for (i=0; i<n_tr_sel; i++)
        i_red_new[i]=i+n_tr_old;      

   mat_from_ind(&c_red[0],&c_red_old[0][0],&i_red_old[0],n,m,n_tr_old);
   mat_from_ind(&c_red[0],&c_red_new[0][0],&i_red_new[0],n,m,n_tr_sel);

   mat_from_ind(&c[0],&c_red_new[0][0],&i_trial[0],n,m,n_tr_sel);
   concatenate_mat(&c_red[0],&c_red_new[0][0],m,n_tr_old,n_tr_sel);
   ds = ds_score(n_tr_tot,m,&c_red[0],M);

   i_change = 1;
   i_cont = 0; 

   for (i=0; i<n; i++)          /*! definition of the index vector */        
       ind[i] = i;
         
   printf("Neighborhood Local Search \n"); 
   while (i_change == 1)
   {
       i_change = 0;
       i_cont = i_cont + 1;
       for (i=0; i<n_tr_sel; i++)
       {
           printf("%4i \n",i); 
           N_sample = pow(2,i_power);
           if (N_sample > n)
               N_sample = n;

           i_max = i_trial[i];
           row_from_mat(&v_ref[0],&c[0],i_max,m);
           for (j=0; j<n; j++)
           {
               row_from_mat(&v_test[0],&c[0],j,m);  
               d[j] = euclid(m,v_ref,v_test);                
           }             
           sort_ind(&d[0],&ind[0],n);
           for (j=0; j<N_sample; j++)
                i_red[j] = ind[j+1];  

           for (j=0; j<N_sample; j++)
           {
               nv = n_in_vec(&i_trial[0],n_tr_sel,i_red[j]);
               if (nv > 0)
                   continue;
               
               i_trial[i] = i_red[j];
               mat_from_ind(&c[0],&c_red_new[0][0],&i_trial[0],n,m,n_tr_sel);
               concatenate_mat(&c_red[0],&c_red_new[0][0],m,n_tr_old,n_tr_sel);
               ds_temp = ds_score(n_tr_tot,m,&c_red[0],M);  
               if (ds_temp > ds)
               {
                  ds = ds_temp;
                  i_max = i_trial[i];
                  i_change = 1;
                  printf("%4i %6.4f \n",j,ds);   
               }     
           }
           i_trial[i] = i_max;
       }
       i_power = i_power - 1;
       if (i_power == 0)  
           i_power = 1;  
       if (i_cont == c_max)  
           i_change = 0;
   }
   return ds;
}

void read_c_input(char *filename,double *c, int n, int m)
{
/*! read_c_input reads the file where are stored the candidates vectors to include in the solution */

    int i,j;

    FILE *fp;
    fp = fopen(filename,"r");

    if (fp != NULL){
       for (j=0; j<n; j++)
       {
           for (i=0; i<m; i++)
               fscanf(fp,"%lf",&c[j*m+i]);      
       }
    } 
    fclose(fp); 
}

void read_partial_solution(int m,int n_tr_tot,int n_tr_sel,double *c_red)
{
/*! read_c_input reads the file where are stored the candidates vectors to include in the solution */

    int i,j,n_sol;
    char filename_v[20] = "solution_v_";
    char filename_extension[8] = ".txt";
    char string_num[10];

    n_sol = n_tr_tot - n_tr_sel;
    
    sprintf(string_num,"%d",n_sol);

    strcat(filename_v,string_num);
    strcat(filename_v,filename_extension);

    FILE *fp;
    fp = fopen(filename_v,"r");

    if (fp != NULL){
       for (j=0; j<n_sol; j++)
       {
           for (i=0; i<m; i++)
               fscanf(fp,"%lf",&c_red[j*m+i]);      
       }
    }
    else
    {
       printf("ERROR MESSAGE: file %s doesn't exist \n",filename_v);  
       exit(0);
    } 
    fclose(fp);
}

void print_solution(int *i_best, double *c_red,int n_tr_sel,int n_tr_tot,int m,char *filename_source,double ds)
{

    int i,j;
    char filename_i[20] = "solution_i_";
    char filename_v[20] = "solution_v_";
    char filename_extension[8] = ".txt";
    char string_num_sel[10];
    char string_num_tot[10];
    char string_ds[16];
    char underscore[10] = "_";

    sprintf(string_num_sel,"%d",n_tr_sel); 
    sprintf(string_num_tot,"%d",n_tr_tot);
    sprintf(string_ds,"%f",ds);

    strcat(filename_i,string_num_sel);
    strcat(filename_i,underscore);
    strcat(filename_i,string_num_tot);   
    strcat(filename_i,filename_extension);
    strcat(filename_v,string_num_tot);
    strcat(filename_v,filename_extension);

    FILE *fp;
    fp = fopen(filename_i,"w");

    fprintf(fp,"Source of the candidates: %s \n",filename_source);
    fprintf(fp,"Total number of elements in the solution: %s \n",string_num_tot);
    fprintf(fp,"Dissimilarity Score = %s \n",string_ds);
    for (i=0; i<n_tr_sel; i++)
        fprintf(fp,"%8i \n",i_best[i]);

    fclose(fp); 

    FILE *fp2;
    fp2 = fopen(filename_v,"w");

    for (i=0; i<n_tr_tot; i++)
    {
        for(j=0; j<m; j++)
        {
           fprintf(fp2,"%6.4f ",c_red[i*m+j]);  
        }
        fprintf(fp2,"\n");
    } 
    fclose(fp2);
}

int main ()
{

   double **c;

   int i,j,j2,m,n,n_tr_sel,n_tr_tot,M,N_cycles, i_cum;
   int i_power_loc, c_max_loc, i_power_scram,c_max_scram;
   double r_basin_c,r_basin_ini;
   char filename_input[40];
 
   FILE *fp;
   fp = fopen("input.gra","r");

   if (fp != NULL){
       fscanf(fp,"%s %*[^\n]s",filename_input); 
       fscanf(fp,"%i %*[^\n]s",&N_cycles);      
       fscanf(fp,"%i %*[^\n]s",&n);
       fscanf(fp,"%i %*[^\n]s",&m); 
       fscanf(fp,"%i %*[^\n]s",&n_tr_sel);
       fscanf(fp,"%i %*[^\n]s",&n_tr_tot);
       fscanf(fp,"%i %*[^\n]s",&M);
       fscanf(fp,"%i %*[^\n]s",&i_power_loc);
       fscanf(fp,"%i %*[^\n]s",&c_max_loc);
       fscanf(fp,"%i %*[^\n]s",&i_power_scram);    
       fscanf(fp,"%i %*[^\n]s",&c_max_scram); 
       fscanf(fp,"%lf %*[^\n]s",&r_basin_c); 
       fscanf(fp,"%lf %*[^\n]s",&r_basin_ini);   
       fscanf(fp,"%i %*[^\n]s",&i_cum);
    } 
    fclose(fp); 

    c = (double **) malloc(n*m*sizeof(double*));
    for (i=0; i<n; i++)
        c[i] = (double*)malloc(sizeof(double)*m); 

        printf("N_cycles                             = %i \n",N_cycles);      
        printf("n (size of basin)                    = %i \n",n);
        printf("m (dimensions of the vectors)        = %i \n",m);  
        printf("n_tr_sel (size of the training set)  = %i \n",n_tr_sel);
        printf("n_tr_tot (size of the training set)  = %i \n",n_tr_tot);
        printf("M (N_loc in dissimilarity score)     = %i \n",M);
        printf("i_power_loc                          = %i \n",i_power_loc);
        printf("c_max_loc                            = %i \n",c_max_loc);
        printf("i_power_scram                        = %i \n",i_power_scram);    
        printf("c_max_scram                          = %i \n",c_max_scram); 
        printf("r_basin_c                            = %f \n",r_basin_c); 
        printf("r_basin_ini                          = %f \n",r_basin_ini);
        printf("i_cum                                = %i \n",i_cum);

   double v_ref[n],v_test[n],d[n],ds_con[N_cycles],ds_loc[N_cycles],ds_loc_loc[N_cycles],c_red[n_tr_tot][m];
   double ds_best;
   int i_trial[n_tr_tot],i_tot[m],i_test[n_tr_tot],i_best[n_tr_tot],i_new[n_tr_sel];
   double c_best[n_tr_tot][m];
   long set_seed;
   gsl_rng *set_rng;

   gsl_rng_env_setup(); 
   set_seed = time(0);
   set_rng = gsl_rng_alloc(gsl_rng_default);
   gsl_rng_set(set_rng, set_seed);    

   read_c_input(filename_input,&c[0][0],n,m);  /*! read the candidate configurations from the file 'candidates.txt'*/

   for (i=0; i<n; i++)          /*! definition of the basin vector */  
        i_tot[i] = i;

   if (i_cum == 0)
   {
      rand_int_v(&i_best[0],&i_tot[0],n_tr_sel,n-1,set_rng);
      mat_from_ind(&c[0][0],&c_red[0][0],&i_best[0],n,m,n_tr_sel);
      ds_best = ds_score(n_tr_sel,m,&c_red[0][0],M);
   }
   else
   {
      read_partial_solution(m,n_tr_tot,n_tr_sel,&c_red[0][0]); 
      ds_best = 0.0;     
   }

   for (i=0; i<N_cycles; i++)
   {

       if (i_cum == 0)
       {
          if (n_tr_sel != n_tr_tot)
          {
             printf("ERROR MESSAGE: for i_cum=0 n_tr_sel must be equal to n_tr_tot \n");  
             exit(0);
          }  
          initialize_c(&i_trial[0],&c[0][0],n,m,M,set_rng,r_basin_ini);
          ds_con[i] = construction(&i_trial[0],&c[0][0],n,m,M,n_tr_sel,set_rng,r_basin_c); 
          local_search_scrambled(&i_trial[0],&c[0][0],n,m,M,n_tr_sel,set_rng,i_power_scram,c_max_scram);
          mat_from_ind(&c[0][0],&c_red[0][0],&i_trial[0],n,m,n_tr_sel);
          ds_loc[i] = ds_score(n_tr_sel,m,&c_red[0][0],M);
          local_search_loc(&i_trial[0],&c[0][0],n,m,M,n_tr_sel,i_power_loc,c_max_loc);
          mat_from_ind(&c[0][0],&c_red[0][0],&i_trial[0],n,m,n_tr_sel);
          ds_loc_loc[i] = ds_score(n_tr_sel,m,&c_red[0][0],M);

          if (ds_loc_loc[i] > ds_best)
          {
             ds_best = ds_loc_loc[i];  
             for (j=0; j<n_tr_sel; j++)
                 i_best[j] = i_trial[j];
          }  

          for (j=0; j<i+1; j++)
              printf("%6.4f %6.4f %6.4f %6.4f \n", ds_con[j],ds_loc[j],ds_loc_loc[j],ds_best); 

          mat_from_ind(&c[0][0],&c_best[0][0],&i_best[0],n,m,n_tr_tot); 
       }
       else
       {
          read_partial_solution(m,n_tr_tot,n_tr_sel,&c_red[0][0]);
          ds_con[i] = construction_cum(&i_new[0],&c_red[0][0],&c[0][0],n,m,M,n_tr_tot,n_tr_sel,set_rng,r_basin_c);   
          ds_loc[i] = local_search_scrambled_cum(&i_new[0],&c_red[0][0],&c[0][0],n,m,M,n_tr_tot,n_tr_sel,set_rng,i_power_scram,c_max_scram);
          ds_loc_loc[i] = local_search_loc_cum(&i_new[0],&c_red[0][0],&c[0][0],n,m,M,n_tr_tot,n_tr_sel,i_power_loc,c_max_loc);
          for (j=0; j<n_tr_sel; j++)
               i_trial[j] = i_new[j];

          if (ds_loc_loc[i] > ds_best)
          {
             ds_best = ds_loc_loc[i];  
             for (j=0; j<n_tr_sel; j++)
                 i_best[j] = i_trial[j];

             for (j=0; j<n_tr_tot; j++)
             {
                 for (j2=0; j2<m; j2++)
                      c_best[j][j2] = c_red[j][j2]; 
             } 
          }  

          for (j=0; j<i+1; j++)
              printf("%6.4f %6.4f %6.4f %6.4f \n", ds_con[j],ds_loc[j],ds_loc_loc[j],ds_best); 

       }
   }

   print_solution(&i_best[0],&c_best[0][0],n_tr_sel,n_tr_tot,m,filename_input,ds_best);    /* da rivedere per considerare il caso che selezioni cumulative provengano da diversi bacini */  
   return 0;      
}
