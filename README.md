# LRR-DE
## Linear Ridge Regression - Differential Evolution

This is a set of programs to perform force field training for Classical Molecular Dynamics simulation,
including utilities for parsing input and output files by different molecular simulation
programs.
The procedure is based on the work 
**Fracchia F, Del Frate G, Mancini G, Rocchia W, Barone V. Force Field Parametrization of Metal Ions From Statistical Learning Techniques. Journal of Chemical Theory and Computation. 2017; doi:10.1021/acs.jctc.7b00779**

Please cite this work if you use this project or part of in a scientific publication.  
Write any questions to:  
- giordano.mancini@sns.it  
- matteo.peluso@sns.it  

