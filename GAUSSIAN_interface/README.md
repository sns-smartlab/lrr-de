
Code Description 
=============================
The code, written in python is divided in three main parts, one dedicated to an ion dissolved in a water solutions, another part that manage heterogeneous solution of residues and the last that works with polymers.

The code needs as input a set of .gro configuration and an input file, input_shelk.txt.

Basic Description
-----------------
The purpouse of this module is to prepare the sampled configurations, obtained by the GRASP sampling, to a Gaussian calculation.

For a water solution it will generate a file .com, for a force calculation, of a shell built as a function of an input number of water molecules, sorted by Euclidean distance.

For a heterogeneous residue solution it will generate a file .com, for a force calculation, of a shell built as a function of an input number of residue molecules, sorted by Euclidean distance.

For a polymer it will generate two file .com, the first for a partial optimization and the second for a force calculation,  of a satureted shell built as a function of an input cutoff radius.