# Input File for the Gaussian interface:

water_solution # System option, i.e. 'polymer'/'water_solution/hetero_solution'
Cl              # Ion atom type, for polymer atomic type plus atom ID 'ZN4703', for solution just the atom type
1               # Ion index, just for solutions
-1              # Name of restype in case of molecule, if single atom in solution -1
-1              # Number of atoms for residue
1               # ion spin state
-1              # charge system
.gro            # Readable format file of input structures, i.e. '.gro'
-1              # Cutoff, If polymer, -1 if solution
16              # Number of water/residue molecules, If solution, -1 if polymer
4               # Boxsize, nm
/home/matteopeluso/Desktop/gaussian_interface/             # Path for gro configuration inputs
/home/matteopeluso/Desktop/gaussian_interface/                 # Path for .com configurations (destination)
/home/matteopeluso/Desktop/gaussian_interface/                 # Path for .gro configurations (destination)
64              # Memory gaussian calculation in GB 
16              # Number of processors gaussian calculation
PBE1PBE/cc-pvDZ # Functional
am1/cc-pVDZ     # Functional for optimization, just for polymer
2               # number of Fragment, if > 1 write number of atoms, spin and charge for each fragment
1               # number of atoms Fragment 1
1               # ion spin Fragment 1
-1              # charge Fragment 1
48              # number of atoms Fragment 2
1               # ion spin Fragment 2
0               # charge Fragment 2