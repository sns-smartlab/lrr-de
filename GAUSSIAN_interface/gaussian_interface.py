## \file gaussian_interface.py
# Updated to the 8 March 2018
# \brief Documentation for Gaussian Interface
# \author: -- -- --
# \n
# This module takes as inputs an input file, 'input_shelk.txt', which contains the information about the \n
# system we would like to generate, and a series of .gro configurations, extracted by the GRASP sampling.\n
# \n
# If the system is a polymer it will build a saturated shell with a cutoff and generate two .gro files, one for a partial optimization and the second for a force\n
# If the system is a solution it will build a shell with the first N water molecules closer and generate a single .gro file for a force calculation in Gaussian, \n
# with if written in the input file, a division in two fragment, ion and water system with the Counterpoise flag activated \n
# \n
# The input_shelk.txt file has to be formatted in the following way:\n\n
#
# Title\n
# \n
# solution/polymer      # System option, i.e. 'polymer'/'water_solution'/hetero_solution \n
# Cl              	# ion atom type, for polymer atomic type plus atom ID ZN4703, for solution just the atom Cl \n
# 1                     # ion/residue index for solution
# ANI                   # Name of restype in case of hetero solution, if single atom in solution -1
# 14                    # Number of atoms for residue
# 1                  	# ion spin state\n
# -1            	# charge system\n
# .gro              	# Readable format file of input structures, at the moment just the .gro is available\n
# -1                	# Cutoff, If polymer, -1 if solution\n
# 16                	# Number of water molecules, If solution, -1 if polymer\n
# 4                  	# Boxsize, nm\n
# /path/to/gro_input/   # Path for gro configuration inputs\n
# /path/to/com_output/  # Path for .com configurations (destination)\n
# /path/to/gro_output/  # Path for .gro configurations (destination)\n
# 12              	# Memory gaussian calculation in GB\n
# 16                	# Number of processors gaussian calculation\n
# PBE1PBE/cc-pvDZ 	# Functional\n
# am1/cc-pVDZ           # Functional for optimization, just for polymer
# 2               	# number of Fragment, if > 1 write number of atoms, spin and charge for each fragment, just for water solution\n
# 1               	# number of atoms Fragment 1\n
# 1               	# ion spin Fragment 1\n
# -1              	# charge Fragment 1\n
# 48              	# number of atoms Fragment 2\n
# 1               	# ion spin Fragment 2\n
# 0               	# charge Fragment 2\n


import os
from math import sqrt
import numpy as np

# ---------------------------------------------------------

# name of the input file
inputfile = 'input_shelk.txt'
if not (os.path.exists('./' + inputfile)):
    r = 'Error!\n"{}" not found.'.format(inputfile)
    print(r)
    exit()

# --------------------------------------------------------------
# Reads the input file

with open(inputfile, 'r') as f:
    lines = f.readlines()
    ##  Chosen type of system
    sys_kind = lines[2].split()[0]
    ##  Ion atom type
    ref_ATOM = lines[3].split()[0]
    ## Ion index solution
    ind_atom = lines[4].split()[0]
    ## Residue name of reference
    res_name = lines[5].split()[0]
    ## Number of atoms per residue
    num_at_res = int(lines[6].split()[0])
    ##  Spin state system
    spin = lines[7].split()[0]
    ##  Charge system
    charge = lines[8].split()[0]
    ##  Readable format file of input structures
    frm = lines[9].split()[0]
    ##  Cutoff if polymer
    cutoff = float(lines[10].split()[0])
    ##  Number of water molecules if solution
    n_h20 = int(lines[11].split()[0])
    ##  Boxsize in nm
    boxsize = float(lines[12].split()[0])
    ##  Path for gro input
    p_gro = lines[13].split()[0]
    ##  Path for .com destination
    p_gau = lines[14].split()[0]
    ##  Path for .gro destination
    p_gro_dest = lines[15].split()[0]
    ##  Memory for Gaussian calculation
    memory = lines[16].split()[0]
    ##  Number of processors for Gaussian calculation
    processors = lines[17].split()[0]
    ##  Functional
    functional = lines[18].split()[0]
    ## Optimization
    optimization = lines[19].split()[0]
    ##  Number of fragments
    n_fragments = int(lines[20].split()[0])

    if (n_fragments) > 1:
        n_line = 20
        it_frag = n_fragments
        tot_frag = []
        while (it_frag != 0):
            frag = []
            n_line = n_line + 1;
            frag.append(lines[n_line].split()[0])
            n_line = n_line + 1
            frag.append(lines[n_line].split()[0])
            n_line = n_line + 1
            frag.append(lines[n_line].split()[0])
            it_frag = it_frag - 1
            tot_frag.append(frag)
f.close()

# Check *.gro files extracted from GROMACS trajectories path
if not (os.path.exists(p_gro)):
    r = 'Error!\n"{}" not found.'.format(p_gro)
    print(r)
    exit()
# Check Gau.com path --> destination
if not (os.path.exists(p_gau)):
    r = 'Error!\n"{}" not found.'.format(p_gau)
    print(r)
    exit()

# Check shell.gro path --> destination
if not (os.path.exists(p_gro_dest)):
    r = 'Error!\n"{}" not found.'.format(p_gro_dest)
    print(r)
    exit()

sys_kind = sys_kind.lower()

sys_kinds = ('polymer', 'water_solution', 'hetero_solution')

# Check if the system chosen is supported
if sys_kind not in sys_kinds:
    r = 'Error!\nSystem kind "{}" not supported.'.format(sys_kind)
    print(r)
    exit()

# Check the number of chosen water molecules
if sys_kind == 'water_solution' and n_h20 <0 or functional == '':
    r = 'Error!\n didn\'t insert a correct water solution input file'
    print(r)
    exit()

# Check the hetero solution input parameteres
if sys_kind == 'hetero_solution' and n_h20 <0 or functional == '' or res_name == '' :
    r = 'Error!\n didn\'t insert a correct hetero solution input file  '
    print(r)
    exit()

# Check the cutoff for polymers
if sys_kind == 'polymer' and cutoff <= 0 or functional == '' or optimization == '' :
    r = 'Error!\n didn\'t insert a correct polymer input file.'
    print(r)
    exit()

# ------------------------------------------------------------
# polymers variables
## Atoms
a_d = ['zn', 'mg', 'cl', 'br', 'fe', 'na', 'ni', 'ca']

## Add in no_std_res any metal ion and solvent molecules included in your system
no_std_res = ['ZN', 'IDR', 'CL', 'SOL', 'MOL']

## Add neg_res any negative-charged residues included in your system, if not present
neg_res = ['ASP', 'GLU', 'CL', 'IDR']

## Possible residues
pos_res = ['LYS', 'ARG']

## Cations
di_cations = ['FE', 'MG', 'NI', 'ZN', 'CA']


# Heterogenous solution variables
## Dictionary atoms --> weight
weight = {'C':12.0116, 'H':1.00811, 'N':14.00728, 'CA':12.0116,'HA':1.00811}


## Dictionary from gromacs to gaussian
gro_to_gau = {'CA':'C','HA':'H','NA':'N','C':'C','H':'H','N':'N'}

# --------------------------------------------------------------
# Functions definition

def distance(a, b):
    """
    Computes euclidean distance between atoms

    @param a list/array of coordinates
    @param b list/array of coordinates

    Returns the Euclidean distance between a and b
    """
    ##\callgraph

    d = float(
        sqrt((float(a[0]) - float(b[0])) ** 2 + (float(a[1]) - float(b[1])) ** 2 + (float(a[2]) - float(b[2])) ** 2))
    return d


def LOAD_LIST(path, frm):
    """
    Load from the directory given by "p" the path of all the input structures in

    @param path path to input structures
    @param frm frames

    Returns returns a list of files
    """
    ##\callgraph
    if path[-1] != '/':
        path = path + '/'
    list_files = []
    os.chdir(path)
    for j in os.listdir(path) :
        if j[-4:] == '{}'.format(frm):
            list_files.append(j)
            
    return list_files


def DO_TOPOL(inpfile):
    """
    Do the topology file of polymer and ligand collecting information about the force field used

    @param inpfile

    Returns the coordinate of the ligand, of the polymer and all the residues
    """
    ##\callgraph
    lig_at, lig_coord = [], []
    pol, pol_at, pol_coord = [], [], []
    all_res, res = [], []
    lig_coord = []
    os.chdir(p_gro)
    with open(inpfile, 'r') as f:
        l = f.readline()
        l = f.readline()
        l = f.readline()
        p = int(l[:5].strip())
        while l:
            # raccolta dati relativa al sistema:
            if len(l.strip().split()) >= 5:
                p = p + 1
                resnum = int(l[:5])
                res.append(resnum)
                all_res.append(resnum)
                res.append(l[5:8].strip())
                # if str(resnum)+l[5:8].strip() == ref_ATOM:
                if l[12:20].replace(' ', '') == ref_ATOM:
                    lig_coord.append(resnum)
                    lig_coord.append(l[8:15].strip())
                    lig_coord.append([l[5:8].strip()])
                    lig_coord.append([[float(l[20:28]), float(l[28:36]), float(l[36:44])]])

                while resnum < p:
                    pol_at.append(l[8:15].strip())
                    pol_coord.append([float(l[20:28]), float(l[28:36]), float(l[36:44])])
                    l = f.readline()
                    try:
                        resnum = int(l[:5])
                    except:
                        break
                res.append(pol_at)
                res.append(pol_coord)
                pol.append(res)
                pol_at, pol_coord, res = [], [], []
            else:
                l = f.readline()
    return lig_coord, pol, all_res


def shell_creator(lig_coord, imm_pol):
    """
    Builds the shell for a polymer

    @param lig_coord coordinates of the ligand
    @param imm_pol

    Returns an array with the shell coordinates and a list of residues
    """
    ##\callgraph
    shell_arr = []
    res_in = []
    a = lig_coord[3][0]
    for res in imm_pol:
        for coord in res[3]:
            dist = distance(a, coord)
            if dist < cutoff:
                if res[0] in res_in:
                    continue
                else:
                    res_in.append(res[0])
                    shell_arr.append(res)
                break
            else:
                continue
    return shell_arr, res_in


def writegro(shell, filegro, length):
    """
    Writes the .gro outputfile of the generated shell

    @param shell
    @param filegro namefile of the .gro file
    @param length length of the shell

    Generates a shell.gro file for each configuration
    """
    ##\callgraph
    ind = 1
    os.chdir(p_gro_dest)
    with open(filegro, 'w') as f:
        f.write('Generated by shelk\n{}\n'.format(length))
        for a in range(len(shell)):
            for i in range(len(shell[a][2])):
                record = '{:>5}{:>3s}{:>7s}{:>5}{:>8.3f}{:>8.3f}{:>8.3f}\n'.format(shell[a][0], shell[a][1],
                                                                                   shell[a][2][i], ind,
                                                                                   shell[a][3][i][0], \
                                                                                   shell[a][3][i][1], shell[a][3][i][2])
                f.write(record)
                ind += 1
        f.write('0.00000   0.00000   0.00000')


def writecom(shell, filecom, sys_kind, index):
    """
    Writes the .com outputfile of the generated shell

    @param shell
    @param filecom namefile of the .com file
    @param length length of the shell

    Generates two shell.com file for each configuration, one that computes a partial optimization and the second one that computes a force calculation
    """
    ##\callgraph
    atoms = []
    q = 0
    os.chdir(p_gau)
    for r in shell:
        if r[1].upper() in neg_res:
            q -= 1
        elif r[1].upper() in pos_res:
            q += 1
        elif r[1].upper() in di_cations:
            q += 2
    with open(filecom, 'w') as f:
        if sys_kind == sys_kinds[0]:
            f.write('%Mem={}GB\n%NProcShared={}\n%chk=snap_opt{}.chk\n'.format(memory,processors,index))
            f.write('#p {}  opt=(tight,ModRedundant)\n\n'.format(optimization))
            f.write('partial optimization\n\n{} {}\n'.format(q, spin))
            for r in shell:
                #       print(r)
                for i in range(len(r[2])):
                    if r[2][i].lower() not in a_d or r[1].lower() not in a_d:
                        line = ' {:>2s}     {:>8.3f} {:>8.3f} {:>8.3f} \n'.format(r[2][i][0], r[3][i][0] * 10,
                                                                                  r[3][i][1] * 10, r[3][i][2] * 10)
                        f.write(line)
                    else:
                        line = ' {:>2s}     {:>8.3f} {:>8.3f} {:>8.3f} \n'.format(r[2][i], r[3][i][0] * 10,
                                                                                  r[3][i][1] * 10, r[3][i][2] * 10)
                        f.write(line)
                    atoms.append(r[2][i])
            for j in range(len(atoms)):
                if atoms[j] != 'OL' and atoms[j] != 'HL':
                    line = '\n X {:>5} F'.format(j + 1)
                    f.write(line)
            f.write('\n\n')
            f.write('--Link1--\n')
            f.write('%Mem={}GB\n%NProcShared={}\n%chk=snap_force{}.chk\n'.format(memory,processors,index))
            f.write('#p {}  force geom=check\n\n'.format(functional))
            f.write('force calculation\n\n')
            f.write(' {} {}\n\n'.format(q, spin))
        elif sys_kind == sys_kinds[1]:
            f.write('#p {}  force\n\n'.format(functional))
            f.write('force calculation\n\n')
            f.write(' {} {}\n'.format(q, spin))
            for r in shell:
                for i in range(len(r[2])):
                    if r[2][i].lower() not in a_d or r[1].lower() not in a_d:
                        line = ' {:>2s}     {:>8.3f} {:>8.3f} {:>8.3f} \n'.format(r[2][i][0], r[3][i][0] * 10,
                                                                                  r[3][i][1] * 10, r[3][i][2] * 10)
                        f.write(line)
                    else:
                        line = ' {:>2s}     {:>8.3f} {:>8.3f} {:>8.3f} \n'.format(r[2][i], r[3][i][0] * 10,
                                                                                  r[3][i][1] * 10, r[3][i][2] * 10)
                        f.write(line)
        f.write('\n')


def CALC_len(sys):
    """
    Evaluate the length of the system

    @param sys

    Returns the length of the system
    """
    ##\callgraph
    length = 0
    for i in range(len(sys)):
        length += len(sys[i][2])
    return length


def SCALE_COORDS(vec_H, vec_X):
    """
    Scales the coordinates of the N/O to be substituted with an H, they have different bond length

    @param vec_X is an array with the coordinated of the N/O atoms to be substituted
    @param vec_H

    Returns a new array with the new scaled coordinates
    """
    ##\callgraph
    k = 0.75  # fattore di scale del legame X-H
    # print(vec_H)
    # print(vec_X)
    new = []
    new.append(vec_X[0] + k * (vec_H[0] - vec_X[0]))
    new.append(vec_X[1] + k * (vec_H[1] - vec_X[1]))
    new.append(vec_X[2] + k * (vec_H[2] - vec_X[2]))
    return new


def saturate(shell, no_std_res, all_res, res_in):
    """
    Saturate the system

    @param shell
    @param no_std_res
    @param all_res
    @param res_in

    Returns the saturated system
    """
    ##\callgraph
    for i in range(len(shell)):
        r = shell[i]
        if r[1] not in no_std_res:
            res_id = pol.index(r)
            if r[0] - 1 in all_res:
                if r[0] - 1 not in res_in:
                    # protono ammina --> inserisco nel residuo C di res precedente e lo sostituisco con H
                    r[2].append('HL')
                    #         print(r[0])
                    new_V = SCALE_COORDS(pol[res_id - 1][3][-2], r[3][0])
                    #         r[3].append(pol[res_id-1][3][-2])
                    r[3].append(new_V)
            if r[0] + 1 in all_res:
                if r[0] + 1 not in res_in:
                    # idrolizzo C=O
                    r[2].append('OL')
                    r[3].append(pol[res_id + 1][3][0])
                    r[2].append('HL')
                    r[3].append(pol[res_id + 1][3][1])


def parse_gro_res(grofile, r, num_at_res,ind_atom):
    '''
    Function that parse a solution .gro file with a molecule

    @param grofile
    @param res
    @param num_at_res

    Returns a list with the coordinate of the reference residue and the other residues
    '''

    with open(p_gro + grofile,'r') as f:
        mol = []
        n_atom = 1;
        res_aux = []
        res_list = []
        res = str(ind_atom)+str(r)
        for i_line, line in enumerate(f.readlines()):
            if line.strip().split()[0] == res:
                prv = []
                prv.append(line.strip().split()[1])
                prv.append(line.strip().split()[3])
                prv.append(line.strip().split()[4])
                prv.append(line.strip().split()[5])
                mol.append(prv)
            if line.strip().split()[0] != res and len(line.strip().split()) > 3 and i_line != 0:
                prv = []
                prv.append(line.strip().split()[1]) # atomo
                prv.append(line.strip().split()[3]) # x
                prv.append(line.strip().split()[4]) # y
                prv.append(line.strip().split()[5]) # z
                res_aux.append(prv)
            if len(res_aux) == len(mol) and res_aux != []:
                res_list.append(res_aux)
                res_aux = []
    f.close()
    return mol, res_list


def centre_of_mass(molecule):
    '''
    Evaluate the centre of mass of a molecule
    
    @param molecule: 
    @return the centre of mass of a molecule
    '''
    cdmx = 0
    cdmy = 0
    cdmz = 0
    weight_tot = 0
    
    for atom in molecule:
        if atom[0] in weight:
            cdmx = cdmx + float(atom[1])*weight[atom[0]]
            cdmy = cdmy + float(atom[2])*weight[atom[0]]
            cdmz = cdmz + float(atom[3])*weight[atom[0]]
            weight_tot = weight_tot + weight[atom[0]]
    
    return [cdmx,cdmy,cdmz]
       
    
def parse_gro(grofile, name_ion, ind_ion):
    '''
    Function that parse a solution .gro file

    @param grofile
    @param name_ion name ion
    @param ind_ion index ion
    Returns two list one with the coordinate of the ion and the second one with the coordinates of the water molecules
    '''
    ##\callgraph
    hw2_flag = False
    atoms = []

    with open(p_gro + grofile, 'r') as f:
        tmp_atm = []
        for line in f.readlines():
            if hw2_flag:
                atoms.append(tmp_atm)
                tmp_atm = []
                hw2_flag = False
            if len(line.strip().split()) == 6:
                if line.strip().split()[1] == name_ion and ind_ion in line.strip().split()[0]:
                    prv = []
                    prv.append(line.strip().split()[3])
                    prv.append(line.strip().split()[4])
                    prv.append(line.strip().split()[5])
                    ion.append(prv)
                if line.strip().split()[1] == 'OW':
                    prv = []
                    prv.append(line.strip().split()[3])
                    prv.append(line.strip().split()[4])
                    prv.append(line.strip().split()[5])
                    tmp_atm.append([prv])
                if line.strip().split()[1] == 'HW1':
                    prv = []
                    prv.append(line.strip().split()[3])
                    prv.append(line.strip().split()[4])
                    prv.append(line.strip().split()[5])
                    tmp_atm.append([prv])
                if line.strip().split()[1] == 'HW2':
                    prv = []
                    prv.append(line.strip().split()[3])
                    prv.append(line.strip().split()[4])
                    prv.append(line.strip().split()[5])
                    tmp_atm.append(prv)
                    hw2_flag = True

    f.close()
    return atoms, ion


# --------------------------------------------------------------
# Heterogeneous Solution
if sys_kind == 'hetero_solution':
    os.chdir(p_gro)
    is_there = False
    for filename in os.listdir(p_gro):
        # Check if there're gro configuration on the chosen directory
        if filename[-4:] == '{}'.format(frm):
            is_there = True
    if is_there == False:
        print('There are no gro configuration file in the chosen directory, change the path')
        exit()

    i = 0
    for filename in os.listdir(p_gro):
        ref = []
        res = []
        cdm_res = []
        if filename[-4:] == '{}'.format(frm):
            print(filename)
            ref, res = parse_gro_res(filename, res_name, num_at_res,ind_atom)
            print(len(res))
            cdm_ref = centre_of_mass(ref)
            for r in res:
                cdm_res.append(centre_of_mass(r))
            distances = []
            for re_id, cdi in enumerate(cdm_res):
                distances.append([distance(cdm_ref,cdi),re_id])
            distances.sort()  # sort the distances
            fin_list = distances[:n_h20]
            #print(fin_list)
            shell_gro = []
            shell_com = []
            it = 2;
            for j in range(n_h20):
                pos = fin_list[j][-1]

                # Get the n_h20 residues coordinates
                for at,atm in enumerate(res[j]):
                    shell_gro.append('{:>5}{:<5}{:>5}{:>5}{:>8}{:>8}{:>8}\n'.format(str(j + 2), res_name,
                                                                                      str(res[pos][at][0]),
                                                                                      it+num_at_res-1,
                                                                                      res[pos][at][1],
                                                                                      res[pos][at][2],
                                                                                      res[pos][at][3]))

                    shell_com.append('{:>5}{:8.3f}{:8.3f}{:8.3f}'.format(gro_to_gau[str(res[pos][at][0])],
                                                          10 * float(res[pos][at][1]),
                                                          10 * float(res[pos][at][2]),
                                                          10 * float(res[pos][at][3])))

                    it = it + 1

            os.chdir(p_gro_dest)
            if os.path.exists(filename[:-4] + '_cluster_' +str(i) + '.gro'):
                os.remove(filename[:-4] + '_cluster_' + str(i) + '.gro')
            with open(filename[:-4] + '_cluster_' + str(i) + '.gro', 'x') as f:
                f.write(' Heterogeneous Solution \n')
                f.write(' {}\n'.format(n_h20 *num_at_res + num_at_res))
                it = 0
                for it, at in enumerate(ref):
                    f.write('{:>5}{:<5}{:>5}{:>5}{:>8}{:>8}{:>8}\n'.format(int(ind_atom), res_name.upper(),
                                                                             str(ref[it][0]),
                                                                             str(it+1),
                                                                             ref[it][1],
                                                                             ref[it][2],
                                                                             ref[it][3]))
                for line in shell_gro:
                    f.write(line)
                f.write('   {}   {}   {}'.format(boxsize, boxsize, boxsize))
            f.close()


            # COM WRITE
            os.chdir(p_gau)
            if os.path.exists(filename[:-4] + '_cluster_' + str(i) + '.com'):
                os.remove(filename[:-4] + '_cluster_' + str(i) + '.com')
            with open(filename[:-4] + '_cluster_' + str(i) + '.com', 'x') as f:
                f.write('%Mem={}GB\n'.format(memory))
                f.write('%NProcShared={}\n'.format(processors))
                f.write('%chk=snap' + str(i) + '.chk\n')
                f.write('#p {} force \n\n'.format(functional,))
                f.write('forces on heterogenous sys comput - NRES: {}\n\n'.format(n_h20))
                f.write('  {} {}\n'.format(charge, spin))

                for it, at in enumerate(ref):
                    f.write('{:>5}{:8.3f}{:8.3f}{:8.3f}\n'.format(gro_to_gau[str(ref[it][0])],
                                                          10 *float(ref[it][1]),
                                                          10 *float(ref[it][2]),
                                                          10 *float(ref[it][3])))



                for line in shell_com:
                    f.write('{}\n'.format(line))
                f.write('\n')
            f.close()
            i = i + 1;







# Water Solution
if sys_kind == 'water_solution':
    os.chdir(p_gro)
    is_there = False
    for filename in os.listdir(p_gro):
        # Check if there're gro configuration on the chosen directory
        if filename[-4:] == '{}'.format(frm):
            is_there = True
    if is_there == False:
        print('There are no gro configuration file in the chosen directory, change the path')
        exit()

    i = 0
    for filename in os.listdir(p_gro):
        ion = []
        waters = []
        if filename[-4:] == '{}'.format(frm):
            print(filename)
            atoms, ion = parse_gro(filename, ref_ATOM, ind_atom)
            distances = []
            cluster = []
            dis_flag = False
            list_ind = []
            # Compute distances
            for j in range(len(atoms)):
                dis = []

                dis_tmp = distance(ion[0], atoms[j][0][0])
                if dis_tmp > boxsize:
                    dis_tmp = dis_tmp - boxsize
                else:
                    dis.append(dis_tmp)  # dis --> ion oxigen

                dis_tmp = distance(ion[0], atoms[j][1][0])
                if dis_tmp > boxsize:
                    dis_tmp = dis_tmp - boxsize
                else:
                    dis.append(dis_tmp)  # dis --> ion hidrogen

                dis_tmp = distance(ion[0], atoms[j][2])
                if dis_tmp > boxsize:
                    dis_tmp = dis_tmp - boxsize
                else:
                    dis.append(dis_tmp)  # dis --> ion hidrogen

                distances.append([np.amin(dis),j])
            distances.sort()  # sort the distances
            # print(distances)

            fin_list = distances[:n_h20]
            shell_gro = []
            shell_com = []
            it = 2;
            for j in range(n_h20):
                pos = fin_list[j][-1]

                # Get the n_h20 coordinates

                shell_gro.append('{:>5}{:<5}{:>5}{:>5}{:8.3f}{:8.3f}{:8.3f}\n'.format(str(j + 2), 'SOL',
                                                                                      'OW',
                                                                                      it,
                                                                                      float(atoms[pos][0][0][0]),
                                                                                      float(atoms[pos][0][0][1]),
                                                                                      float(atoms[pos][0][0][2])))

                shell_com.append('{:>5}{:8.3f}{:8.3f}{:8.3f}'.format('O',
                                                          10 * float(atoms[pos][0][0][0]),
                                                          10 * float(atoms[pos][0][0][1]),
                                                          10 * float(atoms[pos][0][0][2])))

                it = it + 1
                shell_gro.append('{:>5}{:<5}{:>5}{:>5}{:8.3f}{:8.3f}{:8.3f}\n'.format(str(j + 2), 'SOL',
                                                                                      'HW1',
                                                                                      it,
                                                                                      float(atoms[pos][1][0][0]),
                                                                                      float(atoms[pos][1][0][1]),
                                                                                      float(atoms[pos][1][0][2])))

                shell_com.append('{:>5}{:8.3f}{:8.3f}{:8.3f}'.format('H',
                                                          10 * float(atoms[pos][1][0][0]),
                                                          10 * float(atoms[pos][1][0][1]),
                                                          10 * float(atoms[pos][1][0][2])))
                it = it + 1

                shell_gro.append('{:>5}{:<5}{:>5}{:>5}{:8.3f}{:8.3f}{:8.3f}\n'.format(str(j + 2), 'SOL',
                                                                                      'HW2',
                                                                                      it,
                                                                                      float(atoms[pos][2][0]),
                                                                                      float(atoms[pos][2][1]),
                                                                                      float(atoms[pos][2][2])))

                shell_com.append('{:>5}{:8.3f}{:8.3f}{:8.3f}'.format('H',
                                                              10 * float(atoms[pos][2][0]),
                                                              10 * float(atoms[pos][2][1]),
                                                              10 * float(atoms[pos][2][2])))
                it = it + 1

            os.chdir(p_gro_dest)
            if os.path.exists(filename[:-4] + '_cluster_' + str(i) + '.gro'):
                os.remove(filename[:-4] + '_cluster_' + str(i) + '.gro')
            with open(filename[:-4] + '_cluster_' + str(i) + '.gro', 'x') as f:
                f.write('Solution \n')
                f.write(' {}\n'.format(n_h20 * 3 + 1))
                f.write('{:>5}{:<5}{:>5}{:>5}{:8.3f}{:8.3f}{:8.3f}\n'.format(int(ind_atom),ref_ATOM.upper() ,
                                                                             ref_ATOM,
                                                                             1,
                                                                             float(ion[0][0]),
                                                                             float(ion[0][1]),
                                                                             float(ion[0][2])))
                for line in shell_gro:
                    f.write("{}".format(line))
                f.write('   {}   {}   {}'.format(boxsize, boxsize, boxsize))
            f.close()

            os.chdir(p_gau)
            if os.path.exists(filename[:-4] + '_cluster_' +str(i) + '.com'):
                os.remove(filename[:-4] + '_cluster_' + str(i) + '.com')
            with open(filename[:-4] + '_cluster_' + str(i) + '.com', 'x') as f:
                f.write('%Mem={}GB\n'.format(memory))
                f.write('%NProcShared={}\n'.format(processors))
                f.write('%chk=snap' + str(i) + '.chk\n')
                if (n_fragments > 1):
                    f.write('#p {} force {} \n\n'.format(functional, 'Counterpoise={}'.format(n_fragments)))
                    f.write('forces on chloride comput - NWATER: {}\n\n'.format(n_h20))
                    spin_charge_line = '  {} {}'.format(charge, spin)
                    spin_charge_fragment = ''
                    for ii in range(n_fragments):
                        spin_charge_fragment = spin_charge_fragment + ' {} {}'.format(tot_frag[ii][2], tot_frag[ii][1])
                    spin_charge_line = spin_charge_line + spin_charge_fragment
                    f.write('{} \n'.format(spin_charge_line))
                    f.write('{}{}    {}   {}   {}\n'.format(ref_ATOM, '(Fragment=1)', 
							 	10 * float(ion[0][0]),
                                                            	10 * float(ion[0][1]),
                                                            	10 * float(ion[0][2])))
                    for line in shell_com:
                        f.write('{}{}   {}   {}   {}\n'.format(line.split()[0], '(Fragment=2)', line.split()[1],
                                                               line.split()[2], line.split()[3]))
                    f.write('\n')
                    f.close()
                    i = i + 1;
                if (n_fragments == 1):
                    f.write('#p {} force  \n\n'.format(functional))
                    f.write('forces on chloride comput - NWATER: {}\n\n'.format(n_h20))
                    f.write(' {} {} \n'.format(charge, spin))
                    f.write('{:>5}{:8.3f}{:8.3f}{:8.3f}\n'.format(ref_ATOM,
							  10 * float(ion[0][0]),
                                                          10 * float(ion[0][1]),
                                                          10 * float(ion[0][2])))
                    for line in shell_com:
                        f.write('{}\n'.format(line))
                    f.write('\n')
                    f.close()
                    i = i + 1;

# ----------------------------------------------
# Polymer

if sys_kind == 'polymer':
    sys_files = LOAD_LIST(p_gro, frm)
    os.chdir(p_gro)
    index_com = 0;
    for inp_file in sys_files:
        print(inp_file) 
        lig_coord, pol, all_res = DO_TOPOL(inp_file)
        if lig_coord == []:
            r = 'ref_ATOM "{}" not found'.format(ref_ATOM)
            print(r)
            exit()
        shell, res_in = shell_creator(lig_coord, pol)

        saturate(shell, no_std_res, all_res, res_in)  # saturazione carbonile/ammina

        length = CALC_len(shell)
        inp_file_code = inp_file[:-4]
        os.chdir(p_gro_dest) 
        writegro(shell, inp_file_code + '_cluster' + str(index_com) + '.gro', length)
        os.chdir(p_gau)
        writecom(shell, inp_file_code + '_cluster' + str(index_com) + '.com', sys_kind, index_com)
        index_com = index_com + 1;