import numpy as np
import os
import time

## @GROMACS INTERFACE
#  It reads Gromacs single .gro configurations extracted from GROMACS trajectories 
#  and generates a .txt file containing the  

## starting time 
t0 = time.time() 
## input file: a list of paths corresponding to single *.gro files extracted from GROMACS trajectories
#listfile = 'list.txt' 
p = './gro_config'
## if the read input path does not exist the program gives an error message and stops
if not(os.path.exists(p)):
    r = 'Error!\n{} not found.'.format(p)
    print(r)
    exit()
## number of atoms of the system - 1 (except the metal ion) 
n_at = 6534 
## size of the output vector
v_len = 200
## readable format file of input structures
frm = 'gro'
## sigma value , it indicates the distance (in nm) from the metal which identifies the most significant zone to sample
sig = 0.2 
## res ID and name of reference atom (i.e., the first column of .gro file)
metal_atom = '1CL'

## load from the directory given by "p" the path of all the input structures in 
def LOAD_LIST(path, frm):
    if path[-1] != '/':
        path = path + '/'
    list_files = []
    a = os.listdir(path)
    for j in a:
        if j[-4:] == '.{}'.format(frm):
            list_files.append(path+j)
    return list_files

## computes euclidean distances between matrix B and vector A for each row of matrix B
def euclid(A, B): 
    NA = np.shape(A)[0]
    NB = np.shape(B)[0]
    NC = np.shape(A)[1]
    if NC != np.shape(B)[1]:
    ## error: mismatch in euclid function
        return 0, 'error' 
    else:
        ## obtain matrix of same dimension of B, where each row is eq to A 
        AA = np.tile(A, (NB, 1)) 
        ## computes euclidean distance between i-th row of AA and i-th row of B
        NN = np.sqrt(np.sum(np.power(AA-B,2), axis=1)) 
        NN = np.reshape(NN, ((n_at,))) 
        ## NN is a 1*n_at vector which stores all the computed euclidean distances
        return 1, NN

## this function reads a single *gro file and extracts atom types and corresponding coordinates
def STORE_coords(sys_file):
    a_types  = []

    N = 1
    M_coords = []
    sys_coords = []
    with open(sys_file, 'r') as f:
        i = 1
        if frm == 'gro':
            l = f.readline()
            l = f.readline()
            l = f.readline()

            while len(l.split()) != 3:

                if l[:8].strip() == metal_atom:
                    M_coords.append(float(l[20:28]))
                    M_coords.append(float(l[28:36]))
                    M_coords.append(float(l[36:44]))
                    M_coords = np.matrix(M_coords)

                else:
                    a_coords = []
                    a_type = l[5:8].strip() + l[8:15].strip()
                    a_types.append(a_type)
                    a_coords.append(i)     
                    a_coords.append(float(l[20:28]))
                    a_coords.append(float(l[28:36]))
                    a_coords.append(float(l[36:44]))
                    sys_coords.append(a_coords)
                    i += 1

                l = f.readline()
    return M_coords, sys_coords, a_types

## creates the dictionary of the internal nomenclature of each atom type in order to assign a unique numeric code to each atom type of the system
## As output, it gives the dictionary "a_types_dict" and a alphabetically-ordered list (named "set_a_types") of the atom types
def DO_Codes(a_types):
    a_types_dict  = {} 
    set_a_types=list(set(a_types))
    set_a_types.sort()
    for i in range(len(set_a_types)):
        a_types_dict[set_a_types[i]] = i
    return a_types_dict, set_a_types

## creates "d_store" matrix of dim N_at X 3
## each row, which is related to an atom of the system, contains: 0) atom type ID, 1) distance (nm) from the metal ion, and 3) corresponding atom type numeric code,
## as dictated by "a_types_dict"
def DO_d_store(M_coords, sys_coords, a_types, a_types_dict):
    d_store = np.zeros((len(sys_coords), 3))
    MAT = np.array(sys_coords) # atom id | xcoord | ycoord | zcoord
    x_conf = MAT[:,1:]
    NN = np.array(euclid(M_coords, x_conf)[1])
    d_store[:,0] = MAT[:,0]
    d_store[:,1] = NN     # d_store = | atom index | distance from Metal | 0 |
    for i in range(np.shape(d_store)[0]):
        d_store[i,2] = a_types_dict[a_types[i]]  # d_store = | atom index | distance from Metal | atom type code |
    return d_store

## at the th-iteration, this function replace the n-th column of "A_MAT" matrix with the distances from metal ion of each atoms of the th-configuration.
## Furthermore, it orders the "d_store" matrix for atom types at first; and secondly, each atom types is ordered from the nearest to the farest atom from the metal ion.
## Corresponding output is "o_mat", of dim N_at X 2, where the distances (ordered as explained before) are in the first column and the ordinal number of each atom type is in the second column. 
## Relative atom type code for each row of "o_mat" is stored in "a_types_mat". 
def LOAD_A_MAT(a_types_dict, d_store, A_MAT):
    o_mat = np.zeros((n_at, 2))
    a_types_mat = np.zeros((n_at,))
    y = 0
    for i in range(len(a_types_dict)):
        a_mat = d_store[d_store[:,2]==i,:]      # estraggo da d_store solo gli atom types "i"
        a_mat = a_mat[np.argsort(a_mat[:,1])]   # ordino gli atomi "i" estratti dal più vicino allo zinco al più lontano"
        o_mat[:,0][y : np.shape(a_mat)[0] + y]= a_mat[:,1]
        o_mat[:,1][y : np.shape(a_mat)[0] + y]= [x for x in range(np.shape(a_mat)[0])] 
        a_types_mat[y : np.shape(a_mat)[0] + y]= a_mat[:,2] # matrice con gli atom code ordinata allo stesso modo di a_mat
        y = np.shape(a_mat)[0] + y
    A_MAT[:,sys_files.index(sys_file)] = o_mat[:,0]
    return o_mat, a_types_mat

## this function transforms the distances of A_MAT according to "sigma" and computes the variance for each ordered-atom type among all the processed configurations
def Calc_V(A_MAT):
    A_MAT_k = np.exp(-(np.square(A_MAT))/(2*sig**2)) 
    A_MAT_km = np.mean(A_MAT_k, axis=1) 
    A_MAT_km = np.reshape(np.repeat(A_MAT_km, len(sys_files)), (np.shape(A_MAT_k)))
    R = np.sum(np.square(A_MAT_k -A_MAT_km), axis=1)/np.shape(A_MAT_k)[1] 
    return R

## it writes the output file (outfile) of the program  
## outfile identifies each atoms according to the its permutational symmetry
def Write_outfile(R, o_mat, a_types_mat, outfile, set_a_types):
    Q=np.vstack((R,o_mat[:,1])) 
    Q=np.vstack((Q,a_types_mat)).T
    Q = Q[np.argsort(Q[:,0])] 
    Q_sel = Q[-v_len:][::-1]  
    with open(outfile, 'w') as f:
        for i in Q_sel:
            j=int([i][0][2])
            k=int([i][0][1])
            s1 = '{:>8s} {:>4d} {:>5d}\n'.format(set_a_types[j], j, k)
            f.write(s1)
        f.write('VECTOR LENGTH: {}'.format(str(v_len)))
    return 0

# procedural part

if __name__ == '__main__':
    sys_files=LOAD_LIST(p, frm)
    ## A_MAT is an empty matrix N_atoms X N_conf;
    ## A_MAT stores the distances of each atom of each configuration from the metal ion
    A_MAT = np.zeros((n_at,len(sys_files)))
    
    ## each gro file in listfile is processed
    for sys_file in sys_files:
        print(sys_file)
        ## if the read conf file does not exist, the program gives an error message and stops
        if not(os.path.exists(sys_file)):
            r = 'Error!\n{} not found.'.format(sys_file)
            print(r)
            exit()
    
        ## M_coords stores the coordinates of the metal ion (i.e., the reference atom)
        ## sys_coords stores the coordinates of the other atoms (i.e., the system)
        ## a_types stores the atom type codes  of the system (atom type code =  residue code plus atom code, e.g. SOLOW or GLYN)
        M_coords, sys_coords, a_types = STORE_coords(sys_file)
        ## if the number of read atoms is not equal to n_at the program gives an error and stop
        if len(sys_coords) != n_at:
            r = "Error:\n{}: number of atoms set by the user does not correspond with input structure!".\
                format(sys_file)
            print(r)
            exit()
    
        a_types_dict, set_a_types = DO_Codes(a_types)
        d_store = DO_d_store(M_coords, sys_coords, a_types, a_types_dict)
     
        o_mat, a_types_mat = LOAD_A_MAT(a_types_dict, d_store, A_MAT)
    
    R = Calc_V(A_MAT)
    Write_outfile(R, o_mat, a_types_mat, 'selected_coords.txt', set_a_types)
    
    diff = time.time() - t0
    print('Computational time Final: {} sec'.format(str(diff)))
