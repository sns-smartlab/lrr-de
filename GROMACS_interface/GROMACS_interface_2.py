import numpy as np
import time
from GROMACS_interface_1 import LOAD_LIST, euclid, STORE_coords, DO_Codes, DO_d_store

## configurations gro file are located in the directory "p"
p = './gro_config'	

## this file carries all the instruction to built the input vector readable by GRASP for each of the considered configuration stored in "p"
inpfile = 'selected_coords.txt'
## readable format file of input structures
frm = 'gro'
## sigma value , it indicates the distance (in nm) from the metal which identifies the most significant zone to sample
sig = 0.2 

t0 = time.time()

## This function loads from "V_file" the information on how the GRASP input file must be written 
def LOAD_data(V_file):
    v_codes = []
    v_order = []

    with open(V_file, 'r') as f:
        l = f.readline()
        while l[:6] != 'VECTOR':
            a, b, c = l.strip().split()
           #v[a] = int(b)                  # dizionario 'ATYPE': ACODE
            v_codes.append(int(b))
            v_order.append(int(c))
            l = f.readline()
        v_len = int(l[14:])
    return v_codes, v_order, v_len

## create the vector for the processed configuration, according to the instructions given in "inpfile" 
def DO_ARR(d_store, v_codes, v_len, v_order):
    arr = np.zeros(v_len)
    for i in range(len(v_codes)):
        a_mat = d_store[d_store[:,2]==v_codes[i],:]
        a_mat = a_mat[np.argsort(a_mat[:,1])] # an ordered M_d2 matrix considering the distance value 
        arr[i]=a_mat[v_order[i],:][1]
    return arr

## "v_codes" is the ordered list of the atom types to be included (in the specified order) in the vector
## "v_order" is the ordered list of the ordinal number (according to the distance of the atom from the metal atom) of the ordered atom type to be included in the vector
## "v_len" is the length of the vector to create 
v_codes, v_order, v_len = LOAD_data(inpfile)

## list containing path of gro configurations to be processed
sys_files =LOAD_LIST(p, frm)

## "v_store" is an empty matrix v_len X N_conf;
## "v_store" stores the distances of the selected atoms (as set by "inpfile") of each configuration from the metal ion
v_store = np.zeros((v_len, len(sys_files)))  

for sys_file in sys_files:
    ## M_coords stores the coordinates of the metal ion (i.e., the reference atom)
    ## sys_coords stores the coordinates of the other atoms (i.e., the system)
    ## a_types stores the atom type codes  of the system (atom type code =  residue code plus atom code, e.g. SOLOW or GLYN)
    M_coords, sys_coords, a_types = STORE_coords(sys_file)
    ## dictionary of the internal nomenclature of each atom type in order to assign a unique numeric code to each atom type of the system
    a_types_dict = DO_Codes(a_types)[0]
    ## "d_store" matrix of dim N_at X 3
    ## each row, which is related to an atom of the system, contains: 0) atom type ID, 1) distance (nm) from the metal ion, and 3) corresponding atom type numeric code,
    ## as dictated by "a_types_dict"
    d_store = DO_d_store(M_coords, sys_coords, a_types, a_types_dict)
    ARR = DO_ARR(d_store, v_codes, v_len, v_order)
    v_store[:,sys_files.index(sys_file)] = ARR

## v_store is transformed applying a Gaussian kernel
v_store_k =  np.exp(-(np.square(v_store))/(2*sig**2))
np.savetxt('d_store.txt', v_store) 
v_aux = np.transpose(v_store_k)
np.savetxt('k_store.txt', v_store_k) 
np.savetxt('k_store_trasp.txt',v_aux)
diff = time.time() - t0
print('Computational time Final: {} sec'.format(str(diff)))

