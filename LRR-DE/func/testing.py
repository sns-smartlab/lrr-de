import os
import numpy as np
from tool import *
import matplotlib.pyplot as plt

from sklearn import metrics 
import sklearn
import seaborn as sns

def testing_data(data_set, params, plot = 'y'):
    
    """
        Function which evaluates the MSE/MAE of the test setr
        
        @params data_set
        @params params     ---> New Params
        @params old_params 
        @params plot       --->  flag if 'y' plot results
    """
    
    
        
    if plot == 'y':
        sns.set_context("paper")
        
        count_plot = 0
    if data_set.input_params['n_functions'] in [0]:
        aux = [data_set.input_params['q_value']]
        aux.extend(params.tolist()[0])
        params = np.array(aux)
                
    for i in range(2):
        if i == 0:
            data = data_set.energy_ts
        elif i == 1:
            data = data_set.force_ts
            
        for _n_water in data_set.input_params['N_water']:
            print("SET: {}, # of water molecules {}".format(data.flag, _n_water))
            y_ref  = data.training_set['y_test{}'.format(_n_water)]

            q   = data.test_set['q_test{}'.format(_n_water)]
            c6  = data.test_set['c6_test{}'.format(_n_water)]
            c12 = data.test_set['c12_test{}'.format(_n_water)]             

            
            if data_set.input_params['n_functions'] == 1 :
                Hvs = np.matrix([q, c12, c6]).T   # MxN functionts
            elif data_set.input_params['n_functions'] == 0 :
                Hvs = np.matrix([q, c12, c6]).T         
                
            y_test = np.dot(Hvs, params.T).reshape(np.shape(y_ref)).T

            # OLD PARAMS
            q_old       = data_set.input_params['old_params'][0]
            sigma_old   = data_set.input_params['old_params'][1]
            epsilon_old = data_set.input_params['old_params'][2]

            C12old = (4*(epsilon_old)*(sigma_old**12))**0.5
            C6old  = (4*(epsilon_old)*(sigma_old**6))**0.5

            c_old = np.array([q_old, C12old, C6old])

            H = np.matrix([q, c12, c6]).T
            y_test_old = np.dot(H, c_old.T).T
            y_test_old = np.array(y_test_old)            
                
            mse_old_test = np.mean(np.abs(y_test_old.T - y_test))      

            y_test_s     = np.sort(y_test,axis=0)
            y_ref_s      = np.sort(y_ref, axis=0)
            y_test_old_s = np.sort(y_test_old,axis=0)

            mse_test     = np.mean(np.power(y_test_s - y_ref_s, 2))
            mae_test_s   = np.mean(np.abs(y_ref_s - y_test_s.T))
            mae_test_old = np.mean(np.abs(y_test_old_s - y_ref_s))

            print('MSE (lrr-de) = {}'.format(mse_test))
            print('MAE (opls) = {}'.format(mae_test_old))
            print('MAE (lrr-de) = {}'.format(mae_test_s))
            print("-------")
            if plot == 'y':
                sns.set_context("paper")
                plt.figure(dpi = 100)
                plt.rc('text', usetex=True)
                plt.rc('font', family='serif')
                plt.title(" TEST SET: ${0},{1}$ water molecules".format(data.flag, _n_water))
                plt.plot(y_test,label="lrr-de",marker="*")
                plt.plot(y_ref, label="ref",marker="*")
                plt.plot(y_test_old, label="opls",marker="*")
                plt.legend(loc='upper left')
                if data.flag == "energy":
                    plt.ylabel("Energy - Test set [kj]")
                elif data.flag == "force":
                    plt.ylabel("Force - Test set [kj/mol nm]")
                plt.xlabel("$N_{confs}$")   
                #plt.savefig("set{}.pdf".format(count_plot), bbox_inches='tight')
                count_plot = count_plot + 1


