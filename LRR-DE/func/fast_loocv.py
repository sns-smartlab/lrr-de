from tool import *
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error


############### -------------------------- VIRTUAL SITE ------------------------##################################


def fast_loocv_MO(data_set, new_values):
    """
        This function takes as input a data_set and a set of new values
        and evaluates the Leave One Out Cross Validation Error as
        the equation reported on the article
    """
    y    = data_set.y_sample
    v    = data_set.w_vector
    
    # Non scaled Descriptor
    q   = data_set.q
    c6  = data_set.c6
    c12 = data_set.c12

    # Scaled Descriptor
    mean_q,   sigma_q,   data_std_q   = standardize_data(q)
    mean_c6,  sigma_c6,  data_std_c6  = standardize_data(c6)
    mean_c12, sigma_c12, data_std_c12 = standardize_data(c12)
    
    
    if data_set.nfunctions == 1:
        sigma  = np.array([sigma_q, sigma_c12, sigma_c6])    
        H      = np.matrix([data_std_q, data_std_c12, data_std_c6]).T
    elif data_set.nfunctions == 0:
        sigma  = np.array([sigma_c12, sigma_c6])    
        H      = np.matrix([data_std_c12, data_std_c6]).T
        
    l  = leverage(data_set)
    
    err_loocv = []
    mae       = []
    for c in new_values:
        _err_loocv, _mae = fast_loocv(c, H, y, l, v)
        err_loocv.append(np.asscalar(_err_loocv))
        mae.append(_mae)
    return err_loocv, mae


#@jit(fastmath = True) comportamento strano su np.dot....
def fast_loocv(c, H, y, l, v):
    """
        pancakes
    """
    y_sample_est = np.dot(H,c)
    dev          = np.subtract(y_sample_est, y)
    den          = 1 - l
    loocv_ei     = np.power(np.divide(dev.T, den), 2)
    aux          = np.dot(loocv_ei.T, v)    
    mae          = np.mean(np.abs(dev)) 
    return aux, mae