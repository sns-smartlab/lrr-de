from tool import *
from numba import jit
    
def Normal_solver_MO(data_set, vlambda, flag = "n"):
    """
        This function takes as input a data_set and a set of lambdas 
        and evaluates the Multi Objective Normal Equation as
        the equation reported on the article
    """
    # Non scaled Descriptor
    q   = data_set.q
    c6  = data_set.c6
    c12 = data_set.c12

    # Scaled Descriptor
    mean_q,   sigma_q,   data_std_q   = standardize_data(q)
    mean_c6,  sigma_c6,  data_std_c6  = standardize_data(c6)
    mean_c12, sigma_c12, data_std_c12 = standardize_data(c12)
    

    if data_set.nfunctions == 1:
        sigma  = np.array([sigma_q, sigma_c12, sigma_c6])    
        H      = np.matrix([data_std_q, data_std_c12, data_std_c6]).T
    elif data_set.nfunctions == 0:
        sigma  = np.array([sigma_c12, sigma_c6])    
        H      = np.matrix([data_std_c12, data_std_c6]).T            
    
    
            
    # QM Reference data
    y    = data_set.y_sample

    # Weight matrix
    v = data_set.w_vector
    W = data_set.w_matrix
        
    values  = []
    
    M      = np.size(H,0)
    N_func = np.size(H,1)
    I      = np.eye(N_func, N_func)
    #n      = np.size(vlambda,0)
    
    
    if isinstance(vlambda, list):
        for l in vlambda:
            values.append(solve_normal_eq(H,W,M,l,I,y))  # Normal Equation Multi 
            
    elif isinstance(vlambda, np.ndarray):
        for l in vlambda:
            l = np.asscalar(l)
            values.append(solve_normal_eq(H,W,M,l,I,y))  # Normal Equation Multi Objective
    else:
        l = vlambda
        values.append(solve_normal_eq(H,W,M,l,I,y))  # Normal Equation Multi 
        
    values = np.array(values)
    dim    = np.shape(values) 
    values = np.resize(values,(dim[0],dim[-1]))
    if flag == "n":
        return values
    else:
        return values, sigma

@jit(nopython=True,parallel = True,fastmath = True)
def solve_normal_eq(H,W,M,l,I,y):
    return np.dot(np.dot(np.dot(np.linalg.inv(np.dot(np.dot(H.T,W),H) + 2*M*l*I), H.T),W),y)