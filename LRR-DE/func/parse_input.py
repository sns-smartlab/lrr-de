import numpy as np
from lrr_de_constant import *
import matplotlib.pyplot as plt
import os
from read import *
from distances import *
from tool import *

class parser():
    
    def __init__(self, input_params, flag_files):
        """
            pancakes
        """
        self.input_params = input_params
        self.flag_files   = flag_files
    
    def parse(self):
        try:
            os.chdir(self.input_params['file_dir'])
        except:
            print("change path for the files to be parsed")
        for _n_water in self.input_params['N_water']:
            
            _n_atoms = _n_water*3 + 1
            
            if self.flag_files['topology']:
                self.input_params['topol{}'.format(_n_water)]   = read_topol("topology{}.txt".format(_n_water))
                
            if self.flag_files['energy']:
                self.input_params['energy{}'.format(_n_water)]     = read_ener("complexation{}.txt".format(_n_water),
                                                                               self.input_params['counterpoise'])
                
            if self.flag_files['force']:
                self.input_params['force{}'.format(_n_water)]      = read_forces("forces{}.txt".format(_n_water), _n_atoms)
                
            if self.flag_files['coordinate']:
                self.input_params['coordinates{}'.format(_n_water)] = read_coord("coordinates{}.txt".format(_n_water), _n_atoms)
                
        return self.input_params

    def eval_distances(self):
        """
        """
        self.input_params = self.parse()
        for _n_water in self.input_params['N_water']:
            _n_atoms = _n_water*3 + 1
            
                
            self.input_params['d_vet{}'.format(_n_water)] = distance_ion(mat = self.input_params['coordinates{}'.format(_n_water)], 
                                                                         index  = 1,
                                                                         n_conf = self.input_params['n_conf'],
                                                                         n_atom = _n_atoms)

            self.input_params['d_mat{}'.format(_n_water)] = distance_ion_mat(mat = self.input_params['coordinates{}'.format(_n_water)], 
                                                                         index  = 1,
                                                                         n_conf = self.input_params['n_conf'],
                                                                         n_atom = _n_atoms)
        
        return self.input_params
        
    

    
class input_setup():
    """
        This function take as input the parsed input files, or forces or energies, the evaluated
        distances, the size of training and test set and returns a class 
        
        @input_params['topol ']       = output of parse_input for topology
        @input_params['forces']       = output of parse_input for forces
        @input_params['energy']       = output of parse_input for energy
        @input_params['dis_vet']      = output of distance_ion
        @input_params['dis_mat']      = output of distance_ion_mat
        @input_params['N_train']      = Dimension of the training set
        @input_params['N_test']       = Dimension of the test set
        @input_params['data_type']    = Type of the class output, or 'energy' or 'force'
        @input_params['weight']       = Weight of the class output, default 1
        @input_params['borders']      = Border of the search space, default [10e-15, 10e-1]
        @input_params['n_functions']  = Number of function 
        @input_params['flag']         = Flag for test set and training set
        
    """
    
    def __init__(self, input_params, flag, weight):
        
        self.input_params = input_params
        self.N_train      = input_params['n_train']
        self.N_test       = input_params['n_test']
        self.flag         = flag
        self.training_set = {}
        self.test_set     = {}
        try:
            self.input_params['weight32']       = weight[0]
            self.input_params['weight128']      = weight[1]
        except:
            self.input_params['weight32']       = 1
            self.input_params['weight128']      = 1

    
        if self.flag not in ['energy', 'force']:
            print("Choose a proper data type for the input setup between energy or force: {}".format(self.flag))
            return -1

        if input_params['n_train'] < input_params['n_test'] and input_params['n_train'] > 0:
            print("n_train not consistent:    {}".format(input_params['n_train']))
            return -1
    
        if self.flag == 'energy':
            self.training_set = self.initialize_energy()
            self.training_set = self.eval_energy(flag = "train")
            self.test_set     = self.eval_energy(flag = "test") 
            
        elif self.flag == 'force':
            self.training_set = self.initialize_force()
            self.training_set = self.eval_force(flag = "train")
            self.test_set     = self.eval_force(flag = "test") 
    
    def initialize_energy(self):
        """
            Initialization of the energy
        """          
        if self.input_params['counterpoise'] == False:
            for _n_water in self.input_params['N_water']:
                self.training_set['y_sample{}'.format(_n_water)]     = self.input_params['energy{}'.format(_n_water)][0 : self.N_train   , 0] 
                self.training_set['weights{}'.format(_n_water)]      = self.input_params['weight{}'.format(_n_water)]/np.std(self.input_params['energy{}'.format(_n_water)][: , 0])
        else:                    
            for _n_water in self.input_params['N_water']:
                self.training_set['y_sample{}'.format(_n_water)]     = self.input_params['energy{}'.format(_n_water)][0 : self.N_train   , 1] 
                self.training_set['weights{}'.format(_n_water)]      = self.input_params['weight{}'.format(_n_water)]/np.std(self.input_params['energy{}'.format(_n_water)][: , 1])
                self.training_set['energy{}'.format(_n_water)]       = self.input_params['energy{}'.format(_n_water)]    
                self.training_set['y_test{}'.format(_n_water)]       = self.input_params['energy{}'.format(_n_water)][self.N_train : self.N_train + self.N_test , 0]
                self.training_set['x_sample{}'.format(_n_water)]     = self.input_params['d_vet{}'.format(_n_water)][0 : self.N_train , :]
                self.training_set['x_test{}'.format(_n_water)]       = self.input_params['d_vet{}'.format(_n_water)][self.N_train : self.N_train + self.N_test , :]
                    
        
                    
        self.training_set['flag']         = self.flag
        return self.training_set
            
    def eval_energy(self, flag):
        """
            Function which takes as input the data of our system (output of input_setup()) 
            and returns the estimated values of the energies of each frame as Q, C6, C12
        """
        flags = ["test", "train"]
        if flag not in flags: 
            print("choose a proper flag between {}, {}".format(flags, flag))
        
        for _n_water in self.input_params['N_water']:
            if flag == "test":
                
                dt     = self.training_set['x_test{}'.format(_n_water)]

            elif flag == "train":
                dt     = self.training_set['x_sample{}'.format(_n_water)]

            # Charge Contribution
            q   = k_c*np.multiply(np.power(dt[:,1:], -1), self.input_params['topol{}'.format(_n_water)][0,1:])
            q   = np.sum(q,1)


            # C6 Contribution
            c6  = np.multiply(np.power(dt[:,1:], -6), self.input_params['topol{}'.format(_n_water)][1,1:])
            c6  = np.sum(c6,1)

            # C12 Contribution
            c12  = np.multiply(np.power(dt[:,1:], -12), self.input_params['topol{}'.format(_n_water)][2,1:])
            c12  = np.sum(c12,1)

                        

            if flag == "test":
                self.test_set['q_test{}'.format(_n_water)]   = np.array(q)
                self.test_set['c6_test{}'.format(_n_water)]  = np.array(c6)
                self.test_set['c12_test{}'.format(_n_water)] = np.array(c12)    

            elif flag == "train":
                self.training_set['q_train{}'.format(_n_water)]   = np.array(q)
                self.training_set['c6_train{}'.format(_n_water)]  = np.array(c6)
                self.training_set['c12_train{}'.format(_n_water)] = np.array(c12)

        if flag == "train":
            return self.training_set
        elif flag == "test":
             return self.test_set
        else:
            print("not working here")
            return -1
                    
    def initialize_force(self):

        for _n_water in self.input_params['N_water']:
            d, f = [], []
            for i in np.arange(np.size(self.input_params['d_vet{}'.format(_n_water)],0)):
                aux = np.array([])
                d.append(np.append(aux, [self.input_params['d_mat{}'.format(_n_water)][i,:,0] , self.input_params['d_vet{}'.format(_n_water)][i,:]]))
                aux = np.array([])
                d.append(np.append(aux, [self.input_params['d_mat{}'.format(_n_water)][i,:,1] , self.input_params['d_vet{}'.format(_n_water)][i,:]]))
                aux = np.array([])
                d.append(np.append(aux, [self.input_params['d_mat{}'.format(_n_water)][i,:,2] , self.input_params['d_vet{}'.format(_n_water)][i,:]]))
                f.append(self.input_params['force{}'.format(_n_water)][i, 0 , :])


            f = np.reshape(f,[np.size(f,0)*3])           # Vector of forces applied to the ion
            d = np.array(d)
            self.training_set['x_sample{}'.format(_n_water)]     =  d[0 : self.N_train*3, :]
            self.training_set['y_sample{}'.format(_n_water)]     =  f[0 : self.N_train*3] 
            self.training_set['x_test{}'.format(_n_water)]       =  d[self.N_train*3  : self.N_train*3 + self.N_test*3, :]
            self.training_set['y_test{}'.format(_n_water)]       =  f[self.N_train*3  : self.N_train*3 + self.N_test*3] 
            self.training_set['weights{}'.format(_n_water)]      = self.input_params['weight{}'.format(_n_water)]/np.std(f)
            self.training_set['force{}'.format(_n_water)]        = f

        self.training_set['flag']         = self.flag

        return self.training_set      
             
    def eval_force(self,flag):
        """
            Function which takes as input the data of our system (output of input_setup())
            and returns the estimated values of the forces of each frame as Q, C6, C1
        """
        flags = ["test", "train"]
        if flag not in flags:
            print("choose a proper flag between {}, {}".format(flags, flag))

        for _n_water in self.input_params['N_water']:
            if flag == "test":
                
                x     = self.training_set['x_test{}'.format(_n_water)]
                
            elif flag == "train":
                x     = self.training_set['x_sample{}'.format(_n_water)]

            N_iter = np.size(self.training_set['x_sample{}'.format(_n_water)], 1)/2
            N_iter = int(N_iter)

            dt  = x[:, N_iter : N_iter*2]
            dt  = dt[:,1:]

            dc  = x[:, 0 : N_iter ]
            dc  = dc[:,1:]

            # Charge Contribution
            n_q = -1
            q   = n_q*k_c*self.input_params['topol{}'.format(_n_water)][0,1:]*dc[:,:]*(np.power(dt[:,:], (n_q - 2)))
            q   = np.sum(q,1)


            # C6 Contribution
            n_6 = -6
            c6  = n_6*np.multiply(dc[:,:],(np.power(dt[:,:], (n_6 - 2))))
            c6  = c6* self.input_params['topol{}'.format(_n_water)][1,1:]
            c6  = np.sum(c6,1)

            # C12 Contribution
            n_12 = -12
            c12  = n_12*np.multiply(dc[:,:],np.power(dt[:,:], (n_12 - 2)))
            c12  = c12* self.input_params['topol{}'.format(_n_water)][2,1:]
            c12  = np.sum(c12,1) 

            if flag == "test":
                self.test_set['q_test{}'.format(_n_water)]   = np.array(q)
                self.test_set['c6_test{}'.format(_n_water)]  = np.array(c6)
                self.test_set['c12_test{}'.format(_n_water)] = np.array(c12)      


            elif flag == "train":
                self.training_set['q_train{}'.format(_n_water)]   = np.array(q)
                self.training_set['c6_train{}'.format(_n_water)]  = np.array(c6)
                self.training_set['c12_train{}'.format(_n_water)] = np.array(c12)
                        
        if flag == "train":
            return self.training_set
        elif flag == "test":
            return self.test_set
        else:
            print("not working here")
            return -1
                


class training_set():
    """
        args  = energy_ts, force_ts
    """
    def __init__(self, energy_ts, force_ts):
        """
            Initialization, system sensitive
        """

        self.energy_ts    = energy_ts
        self.e_ts         = energy_ts.training_set
        self.force_ts     = force_ts    
        self.f_ts         = force_ts.training_set
        self.input_params = energy_ts.input_params
            
    def build_weight(self):
        """
            Function which collects in one array all the weight and builds the weight matrix
        """ 
        w_vector = np.array([])
        
        # Energies
        w_e128 = self.energy_ts.training_set['weights128']
        ne_128 = np.size(self.energy_ts.training_set['y_sample128'],0)
        w_e32  = self.energy_ts.training_set['weights32']
        ne_32  = np.size(self.energy_ts.training_set['y_sample32'],0)
        # Forces
        w_f128 = self.force_ts.training_set['weights128']
        nf_128 = np.size(self.force_ts.training_set['y_sample128'],0)
        w_f32  = self.force_ts.training_set['weights32']
        nf_32  = np.size(self.force_ts.training_set['y_sample32'],0)
        
        w_vector = np.hstack([
            w_e128*np.ones(ne_128)*(1/ne_128),
            w_e32*np.ones(ne_32)*(1/ne_32),
            w_f128*np.ones(nf_128)*(1/nf_128),
            w_f32*np.ones(nf_32)*(1/nf_32)])
           
        w_vector = np.array(w_vector)
        w_matrix = np.diag(w_vector)
        return np.array(w_vector), np.matrix(w_matrix)


    def model_descriptor(self):
        """
            Method which collects the non scaled descriptor of our system
        """

        self.q   =  np.hstack([self.e_ts['q_train128'],   self.e_ts['q_train32'],   self.f_ts['q_train128'],   self.f_ts['q_train32']])
        self.c6  = -np.hstack([self.e_ts['c6_train128'],  self.e_ts['c6_train32'],  self.f_ts['c6_train128'],  self.f_ts['c6_train32']])
        self.c12 =  np.hstack([self.e_ts['c12_train128'], self.e_ts['c12_train32'], self.f_ts['c12_train128'], self.f_ts['c12_train32']])

        self.y_sample = np.hstack([self.e_ts['y_sample128'], self.e_ts['y_sample32'], self.f_ts['y_sample128'], self.f_ts['y_sample32']])
        self.w_vector, self.w_matrix = self.build_weight()

        self.borders     = self.energy_ts.input_params['borders']
        self.nfunctions  = self.energy_ts.input_params['n_functions']
        self.q_value     = self.energy_ts.input_params['q_value']