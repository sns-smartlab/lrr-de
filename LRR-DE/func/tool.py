import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from lrr_de_constant import *
from sklearn.metrics import mean_squared_error, r2_score  

def standardize_data(data):
    """
        Function which takes as input a data array and
        returns his mean, standard deviation and
        the data divided by his std
    """
    mean     = np.mean(data)
    sigma    = np.std(data)
    data_std = np.divide(data, sigma)
    return mean, sigma, data_std

def leverage(data_set, flag = "novs"):
    """
        Function which leverage data
    """
    # QM Data
    y    = data_set.y_sample
    # Weight matrix
    v    = data_set.w_vector  
    W    = data_set.w_matrix 
    
    # Non scaled Descriptor
    q   = data_set.q
    c6  = data_set.c6
    c12 = data_set.c12

    # Scaled Descriptor
    mean_q,   sigma_q,   data_std_q   = standardize_data(q)
    mean_c6,  sigma_c6,  data_std_c6  = standardize_data(c6)
    mean_c12, sigma_c12, data_std_c12 = standardize_data(c12)
    

    if data_set.nfunctions == 1:
        H = np.matrix([data_std_q, data_std_c12, data_std_c6]).T   # MxNfunctionts

    elif data_set.nfunctions == 0:
        H = np.matrix([data_std_c12, data_std_c6]).T   # MxNfunctionts
    
   

    n    = np.size(data_std_c12,0)
    V    = np.tile(v,(np.size(H,1),1))
    
    H = np.dot(H,V)
    
    mean = np.mean(H)
    Mean = np.ones(np.shape(H))
    dev  = H - Mean
    num  = np.sum(np.power(dev,2),1)
    den  = np.sum(num)
    
    l = 1/n + num/den
    return l


def eval_r2_mse(data_set, params):
    """
        Evaluate the R2/MSE/MAE of the output
        and compares it with the old values
        
         @params data_set
         @params params    ---> new params
    """
    y_ref    = data_set.y_sample

    # Non scaled Descriptor
    q   = data_set.q
    c6  = data_set.c6
    c12 = data_set.c12
    
    # Old Params
    q_old       = data_set.input_params['old_params'][0]
    sigma_old   = data_set.input_params['old_params'][1]
    epsilon_old = data_set.input_params['old_params'][2]

    C12old = (4*(epsilon_old)*(sigma_old**12))**0.5
    C6old  = (4*(epsilon_old)*(sigma_old**6))**0.5
    
    c_old = np.array([q_old, C12old, C6old])
    H = np.matrix([q, c12, c6]).T
    y_test_old = np.dot(H, c_old.T).reshape(np.shape(y_ref)).T
    y_test_old = np.array(y_test_old)
    
    if data_set.input_params['n_functions'] in [0]:
        y_c        = data_set.input_params['q_value']*data_set.q
        y_c        = y_c.reshape(np.shape(y_test_old))
        y_test_old = y_test_old - y_c
    
        
    if data_set.input_params['n_functions'] == 1 :
        Ht = np.matrix([q, c12, c6]).T   # MxN functionts
    elif data_set.input_params['n_functions'] == 0 :
        Ht = np.matrix([c12, c6]).T
                    
    # Eval metrics lrr-de
    y_test    = np.dot(Ht, params.T).reshape(np.shape(y_ref)).T
    
    r2_lrrde  =  r2_score(y_ref, y_test)
    mse_lrrde = np.mean(np.power(y_test - y_ref, 2))
    mae_lrrde = np.mean(np.abs(y_test - y_ref.T))
    print("-----------------------------")
    print("Statistics:")
    print("-----------------------------")
    print("R2 score:  lrr-de", np.round(r2_lrrde,2))
    print("MSE score: lrr-de", np.round(mse_lrrde,2))
    print("MAE score: lrr-de", np.round(mae_lrrde,2))

    # Eval metrics old params
    r2_old =  r2_score(y_ref, y_test_old)
    mse_old = np.mean(np.power(y_test_old - y_ref, 2))
    mae_old = np.mean(np.abs(y_test_old - y_ref))
    print("-----------------------------")
    print("R2 score:  old params", np.round(r2_old,2))
    print("MSE score: old params", np.round(mse_old,2))
    print("MAE score: old params", np.round(mae_old,2))
    print("-----------------------------")
    
    sns.set_context("paper")
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.figure(dpi = 100)
    plt.title("Sorted input -- output values")
    plt.plot(np.sort(y_ref,axis=0), "-", label="ref" )
    plt.plot(np.sort(y_test,axis=0),"-", label="lrr-de" )
    plt.plot(np.sort(y_test_old,axis=0),"-",  label="opls")
    plt.legend(loc='upper left')
    
    plt.figure(dpi = 100)
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.title("Energy contribution")
    plt.plot(y_ref[0:data_set.input_params['n_train']*2],  "-", label="ref" )
    plt.plot(y_test[0:data_set.input_params['n_train']*2], "-", label="lrr-de" )
    plt.plot(y_test_old[0:data_set.input_params['n_train']*2], "-",  label="opls")
    plt.legend(loc='upper left')      
    plt.xlabel("$N_{conf}$")
    plt.ylabel("Energy [Kj]")
    

    plt.figure(dpi=100)
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.title("Force contribution")
    plt.plot(y_test_old[data_set.input_params['n_train']*2:], "-",  label="opls")
    plt.plot(y_ref[data_set.input_params['n_train']*2:],  "-", label="ref" )
    plt.plot(y_test[data_set.input_params['n_train']*2:], "-", label="lrr-de" )
    
    plt.legend(loc='upper left')
    plt.xlabel("$N_{conf}*3$")
    plt.ylabel("Force [Kj/mol nm]")
    
    plt.show()
    return r2_lrrde, mse_lrrde
