import numpy as np

def distance_ion(mat, n_atom, index, n_conf):
    """
        Function which evaluate the distances between our ion and the water molecules,
        returns a vector
        
        Case index = 1 --> Single ion in aqueous solution
        
        @param mat    = matrix of coordinates
        @param n_atom = number of atoms of the system
        @param index  = index of or the single ion
    """
    
    if index == 1:
        d      = []
        for i in np.arange(n_conf):
            d.append(np.array([np.linalg.norm(mat[int(i),0,:] - dis) for dis in mat[int(i),:,:]]))
        return np.array(d, dtype='float')            

def distance_ion_mat(mat, n_atom, index, n_conf):
    """
        Function which evaluate the distances between our ion and the water molecules,
        returns a matrix
        
        Case index = 1 --> Single ion in aqueous solution
        
        @param mat    = matrix of coordinates
        @param n_atom = number of atoms of the system
        @param index  = index of or the single ion
    """
    if index == 1:
        d      = []
        for i in np.arange(n_conf):
            b  = np.ones([n_atom,3])*mat[int(i), 0, :]
            c  = mat[int(i), :, :] - b
            d.append(c)
        d  = np.array(d)
        return d
