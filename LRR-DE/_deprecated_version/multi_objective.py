import numpy as np
from tool import *
import matplotlib.pyplot as plt 

def Normal_solver_MO(data_set, vlambda, flag = "n"):
    """
        This function takes as input a data_set and a set of lambdas 
        and evaluates the Multi Objective Normal Equation as
        the equation reported on the article
    """
    
    if data_set.nfunctions == 3:
        # Non scaled Descriptor
        q   = data_set.q
        c6  = data_set.c6
        c12 = data_set.c12

        # Scaled Descriptor
        mean_q,   sigma_q,   data_std_q   = standardize_data(q)
        mean_c6,  sigma_c6,  data_std_c6  = standardize_data(c6)
        mean_c12, sigma_c12, data_std_c12 = standardize_data(c12)

        sigma  = np.array([sigma_q, sigma_c12, sigma_c6])

        # QM Reference data
        y    = build_ysample(data_set)

        # Weight matrix
        v, W = build_wmatrix(data_set)

        H = np.matrix([data_std_q, data_std_c12, data_std_c6]).T   # MxNfunctionts
        
    elif data_set.nfunctions == 2:            
        # Non scaled Descriptor
        c6  = data_set.c6
        c12 = data_set.c12

        # Scaled Descriptor
        mean_c6,  sigma_c6,  data_std_c6  = standardize_data(c6)
        mean_c12, sigma_c12, data_std_c12 = standardize_data(c12)

        sigma  = np.array([sigma_c12, sigma_c6])

        # QM Reference data
        y    = build_ysample(data_set)

        # Weight matrix
        v, W = build_wmatrix(data_set)

        H = np.matrix([data_std_c12, data_std_c6]).T   # MxNfunctionts
        
    M      = np.size(H,0)
    N_func = np.size(H,1)
    I      = np.eye(N_func, N_func)
    #n      = np.size(vlambda,0)

    values  = []
    
    if isinstance(vlambda, list):
        for l in vlambda:
            if data_set.model == 'SO':
                values.append(np.dot(np.dot(np.linalg.inv(np.dot(H.T,H) + 2*M*l*I), H.T),y))  # Normal Equation Multi Objective
            elif data_set.model == 'MO':
                values.append(np.dot(np.dot(np.dot(np.linalg.inv(np.dot(np.dot(H.T,W),H) + 2*M*l*I), H.T),W),y))  # Normal Equation Multi 
            
    elif isinstance(vlambda, np.ndarray):
        for l in vlambda:
            l = np.asscalar(l)
            if data_set.model == 'SO':
                values.append(np.dot(np.dot(np.linalg.inv(np.dot(H.T,H) + 2*M*l*I), H.T),y))  # Normal Equation Multi Objective
            elif data_set.model == 'MO':
                values.append(np.dot(np.dot(np.dot(np.linalg.inv(np.dot(np.dot(H.T,W),H) + 2*M*l*I), H.T),W),y))  # Normal Equation Multi Objective
    else:
        l = vlambda
        if data_set.model == 'SO':
            values.append(np.dot(np.dot(np.linalg.inv(np.dot(H.T,H) + 2*M*l*I), H.T),y))  # Normal Equation Multi Objective
        elif data_set.model == 'MO':
            values.append(np.dot(np.dot(np.dot(np.linalg.inv(np.dot(np.dot(H.T,W),H) + 2*M*l*I), H.T),W),y))  # Normal Equation Multi 
        
    values = np.array(values)
    dim    = np.shape(values) 
    values = np.resize(values,(dim[0],dim[-1]))
    if flag == "n":
        return values
    else:
        return values, sigma

def fast_loocv_MO(data_set, new_values):
    """
        This function takes as input a data_set and a set of new values
        and evaluates the Leave One Out Cross Validation Error as
        the equation reported on the article
    """
    
    if data_set.nfunctions == 3:
        # Non scaled Descriptor
        q   = data_set.q
        c6  = data_set.c6
        c12 = data_set.c12

        # Scaled Descriptor
        mean_q,   sigma_q,   data_std_q   = standardize_data(q)
        mean_c6,  sigma_c6,  data_std_c6  = standardize_data(c6)
        mean_c12, sigma_c12, data_std_c12 = standardize_data(c12)

        sigma  = np.array([sigma_q, sigma_c12, sigma_c6])

        # QM Reference data
        y    = build_ysample(data_set)

        # Weight matrix
        v, W = build_wmatrix(data_set)

        H = np.matrix([data_std_q, data_std_c12, data_std_c6]).T   # MxNfunctionts
        n_set =  np.size(y,0)
        l  = leverage(data_set)
    
    elif data_set.nfunctions == 2:
        # Non scaled Descriptor
        c6  = data_set.c6
        c12 = data_set.c12

        # Scaled Descriptor
        mean_c6,  sigma_c6,  data_std_c6  = standardize_data(c6)
        mean_c12, sigma_c12, data_std_c12 = standardize_data(c12)

        sigma  = np.array([sigma_c12, sigma_c6])

        # QM Reference data
        y    = build_ysample(data_set)

        # Weight matrix
        v, W = build_wmatrix(data_set)

        H = np.matrix([data_std_c12, data_std_c6]).T   # MxNfunctionts
        l  = leverage(data_set)
            
    err_loocv = []
    for c in new_values:
        y_sample_est = np.dot(H,c)
        dev          = np.subtract(y_sample_est, y)
        den          = 1 - l
        loocv_ei     = np.power(np.divide(dev.T,den), 2)
        aux          = (np.dot(loocv_ei.T, v))
        err_loocv.append(np.asscalar(aux))
        
    return err_loocv 


def eval_lrrde(data_set):
    """
        pancakes
    """
    def Cost(x):
        """
            Function which evaluate the cost function of the MO-LRR-DE
            and returns a list of lambdas
            
        """        
        
        new_params = Normal_solver_MO(data_set, x)
        loocve     = fast_loocv_MO(data_set, new_params)
        
        return loocve
    
    
    def procedure_lrr_de(borders, para = [10, 0.7, 0.9, 50]):
        """
            Even more pancakes
        """
    # DE default params    
        # DE parameter --> population, (10 to 25)
        n = para[0]           
        # DE parameter --> scaling, (0.5 to 0.9) 
        F = para[1]           
        # DE parameter --> crossover probability
        Cr = para[2]          
        # DE parameter --> number of generations
        step_delta = para[3]  
        
    # Iteration Params
        
        # Stop tolerance
        tol     = 10e-5
        k_max   = 500
        k       = 0
        delta_f = 2*tol
        
        # Initialization Number of functions 
        N_iter = 0            
        
        # Dimension of the search space
        d = np.shape(borders)[0]
       
        # definition of lower and upper bounds vectors
        Lb = borders[:,0]
        Ub = borders[:,1]
     
        # initialize the population with simple random sampling
        Lb_rep = np.tile(Lb.T, (n, 1))
        Ub_rep = np.tile(Ub.T, (n, 1))
        R_init = np.random.rand(n,d)
        
        x = Lb_rep + np.multiply((Ub_rep - Lb_rep),R_init)
        y = np.array(Cost(x))
    
        # Find the current best
        best  = np.min(y)
        I     = np.argmin(y)
        I_one = np.mean(y)
        
        # start the iterations by differential evolution
        old_best = best
        II = 0
        
        # Prepare the matrix nd_M of the random indices
        #while delta_f > tol and k < k_max:

        while  k < k_max :
            #print("Step number {}".format(k))
            II    = II + 1
            k     = k + 1
            index = []
            for i in range(n):
                aux  = [ii for ii in range(n)]
                aux.remove(i)
                aux  = np.random.permutation(aux)
                index.append(aux)
            nd_M = np.matrix(index) 
            # metric for the termination
            if II % step_delta == True and k > 10 :
                # the algorithm performs a number of "step_delta" cycles
                I_value  = np.mean(y)
                delta_f  = abs((I_value - I_one))/(0.5*(abs(I_value) + abs(I_one)))
                I_one    = I_value 
                best  = np.min(y)
                #print(delta_f, x[np.argmin(y),:], best, k)
                
            
            # creation of the donor vector (i.e., the mutant vector)
            i_donor = nd_M[:,0:3]
            a       = np.array(x[i_donor[:,0]])
            b       = np.array(x[i_donor[:,1]])
            c       = np.array(x[i_donor[:,2]])
            v_donor = np.matrix(a + F*(b - c))
            
            if np.shape(v_donor)[0] == 1: 
                v_donor = v_donor.T
    
            # CROSSOVER (BINOMIAL SCHEME)
            R_cross =  np.random.rand(n,d)
            I_Cr    = (R_cross < Cr)
            x_new   = np.multiply(v_donor, I_Cr) + np.multiply((I_Cr == 0), x)
            #x_new   = boundary_conditions(x_new, borders, 1)
            
            # Selection
            y_new           = np.array(Cost(x_new))
            v_s             = [y_new < y]
            #print("{}".format(x_new[tuple(v_s)]))
            #print("----------------")
            x[tuple(v_s)]   = x_new[tuple(v_s)]
            y[tuple(v_s)]   = y_new[tuple(v_s)]
            
            # Find the current best
            best  = np.min(y)
            #print(best)
            I     = np.argmin(y)
            xbest = x[I,:]
            my    = np.sum(y)/np.shape(y)[0]
            
        N_iter = II
        return best, xbest, x, y, N_iter
    
    if data_set.nfunctions == 2:
        """
            pancakesssss
        """
        y_c = data_set.q_value*data_set.q
        cont = 0

        for i in range(len(data_set)):
            n_instances = np.shape(data_set[i].y_sample)[0]
            data_set[i].y_sample = data_set[i].y_sample - y_c[cont:cont+n_instances]
            cont = cont + n_instances;

    best, xbest, x, y, N_iter = procedure_lrr_de(data_set.borders)
    vlambda = float(xbest[0])
    values, sigma = Normal_solver_MO(data_set, vlambda, "y")

    params = np.divide(values, sigma)
    print("LOOCV error {}, lambda {}, Numero Iterazioni {}, Parametri {}". format(best, xbest[0], N_iter, params))
    return best, xbest, x, y, N_iter, params

