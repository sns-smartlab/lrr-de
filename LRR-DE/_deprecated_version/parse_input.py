import numpy as np
from lrr_de_constant import *

def parse_input(filename, n_atom, type_input):
    """
        Parse the output file of the matlab interface: coordinatesN.txt forcesN.txt energyN.txt
        topologyN.txt where N is the number of water molecules of the chosen cluster.
        
        @param filename   = name of the file to be parsed
        @param n_atom     = number of atoms of the system
        @param type_input = type of input between coordinates, forces, topology or energy to be parsed
        
        The output for coordinates is converted from Angstrom to nm
        The output for energy is converted from kcal/mol to kJ/mol
        The output for topology is converted from sigma-epsilon to sqrt(c6) and sqrt(c12)
    """
    
    if type_input not in ['coordinates', 'forces', 'energy', 'topology']:
        return "Choose a correct type of file to be parsed between {}".format(['coordinates', 'forces', 'energy', 'topology'])
    
    if n_atom <= 0:
        return "Number of atoms null or inconstitent {}".format(n_atom)
    
    if type_input == 'forces':
        with open(filename,'r') as f:
            lines = f.readlines()
        f.close()
        forces = []
        
        for line in lines:
            if len(line.split()) == 4 and 'forces' not in line:
                forces.append([float(line.split()[1]), \
                               float(line.split()[2]), \
                               float(line.split()[3])])
        forces = np.array(forces)
        n_conf = np.size(forces)/(n_atom*3)
        forces = np.reshape(forces,(int(n_conf),n_atom,3))*conv_force                    # Conversion forces n kJ/mol nm
        return forces
    
    if type_input == 'coordinates':
        with open(filename,'r') as f:
            lines = f.readlines()
        f.close()
        coordinates = []
        for line in lines:
            if len(line.split()) == 4 and 'coo' not in line:
                coordinates.append([float(line.split()[1]), \
                                    float(line.split()[2]), \
                                    float(line.split()[3])])
        coordinates = np.array(coordinates)*0.1
        n_conf = np.size(coordinates)/(n_atom*3)
        coordinates = np.reshape(coordinates,(int(n_conf),n_atom,3))
        return coordinates
    
    if type_input == 'energy':
        with open(filename,'r') as f:
            lines = f.readlines()
        f.close()
        energy = []
        for line in lines:
            if len(line.split()) == 2 and 'raw' not in line:
                energy.append([float(line.split()[0]), \
                               float(line.split()[1])])
        energy = np.array(energy)*conv_kcal_kj                     # Conversion from kcal/mol to kj/mol
        return energy
    
    if type_input == 'topology':
        with open(filename,'r') as f:
            lines = f.readlines()
        f.close()
        aux = []
        for line in lines:
            if len(line.split()) == 6 and 'INDEX' not in line:
                aux.append([float(line.split()[3]), \
                            float(line.split()[4]), \
                            float(line.split()[5])])
        aux      =  np.array(aux)
        q        =  aux[:,0]
        c6       =  np.sqrt(4*aux[:,2]*aux[:,1]**6)  # Sqrt of C6
        c12      =  np.sqrt(4*aux[:,2]*aux[:,1]**12) # Sqrt of C12 
        topology =  np.vstack([q,c6,c12])
        return topology
    

def input_setup(topology, forces, energy, dis_vet, dis_mat, N_train, N_test, data_type, num_nlinpar, nlinparams, linparams, num_linpar, name, weight = 1, nfunctions = 3, borders = np.matrix([10e-15, 10e-1]), q_value = 0):
    
    """
        This function take as input the parsed input files, or forces or energies, the evaluated
        distances, the size of training and test set and returns a class 
        
        @param topology     = output of parse_input for topology
        @param forces       = output of parse_input for forces
        @param energy       = output of parse_input for energy
        @param dis_vet      = output of distance_ion
        @param dis_mat      = output of distance_ion_mat
        @param N_train      = Dimension of the training set
        @param N_test       = Dimension of the test set
        @param data_type    = Type of the class output, or 'energy' or 'force'
        @param weight       = Weight of the class output, default 1
        @param borders      = Border of the search space, default [10e-15, 10e-1]
        @param linparams    = Linear params
        @param num_linpar   = Number of linear params
        @param nlinparams   = Non linear params
        @param num_nlinpar  = Number of non linear params
        
    """
    
    if data_type not in ['energy', 'force']:
        print("Choose a proper data type for the input setup between energy or force:    {}".format(data_type))
        return -1
    
    if N_train < N_test and N_train > 0:
        print("N_train not consistent:    {}".format(N_train))
        return -1
    
    if data_type == 'energy':
        class data():
        
            def __init__(self, topology, energy, dis_vet, N_train, N_test, borders, nfunctions, num_nlinpar, nlinparams, linparams, num_linpar, q_value, name):
            
                self.x_sample     = dis_vet[0 : N_train , :]
                self.y_sample     = energy[0 : N_train   , 1]
                #self.x_test       = dis_vet[N_train : N_train + N_test , :]
                #self.y_test       = energy[N_train : N_train + N_test , 0]
                self.x_test       = dis_vet[:, :]
                self.y_test       = energy[:, 1]
                self.topol        = topology
                self.weights      = weight/np.std(energy[: , 1])
                self.data_type    = "energy"
                self.borders      = borders
                self.nfunctions   = nfunctions
                self.linparams    = linparams
                self.num_linpar   = num_linpar
                self.nlinparams   = nlinparams
                self.num_nlinpar  = num_nlinpar
                self.q_value      = q_value
                self.name         = name
                
            def plot(self, N_train):
                plt.figure()
                plt.plot(self.y_sample,label="Training set energy N train: {}".format(N_train))
                plt.legend()
                plt.show()
            
            def eval_energy(self, flag):
                """
                    Function which takes as input the data of our system (output of input_setup()) 
                    and returns the estimated values of the energies of each frame as Q, C6, C12
                """
                flags = ["test", "train"]
                if flag not in flags:
                    print("choose a proper flag between {}, {}".format(flags, flag))
                    
                if flag == "test":
                    dt = self.x_test
                else:
                    dt = self.x_sample

                # Charge Contribution
                q   = k_c*np.multiply(np.power(dt[:,1:], -1), self.topol[0,1:])
                q   = np.sum(q,1)

                # C6 Contribution
                c6  = np.multiply(np.power(dt[:,1:], -6), self.topol[1,1:])
                #c6  = -np.sum(c6,1)
                c6  = np.sum(c6,1)
                # C12 Contribution
                c12  = np.multiply(np.power(dt[:,1:], -12), self.topol[2,1:])
                c12  = np.sum(c12,1)
                
                if flag == "test":
                    self.q_test   = q
                    self.c6_test  = c6
                    self.c12_test = c12
                    return self.q_test, self.c6_test, self.c12_test
                else:
                    self.q   = q
                    self.c6  = c6
                    self.c12  = c12
                    return self.q, self.c6, self.c12
            
            
        return data(topology, energy, dis_vet, N_train, N_test, borders, nfunctions, num_nlinpar, nlinparams, linparams, num_linpar, q_value, name) 
                
    if data_type == 'force':
        class data():
        
            def __init__(self, topology, forces, dis_vet, dis_mat, N_train, N_test, borders, nfunctions, num_nlinpar, nlinparams, linparams, num_linpar, q_value, name):
                
                d, f = [], []
                for i in np.arange(np.size(dis_vet,0)):
                    aux = np.array([])
                    d.append(np.append(aux, [dis_mat[i,:,0] , dis_vet[i,:]]))
                    aux = np.array([])
                    d.append(np.append(aux, [dis_mat[i,:,1] , dis_vet[i,:]]))
                    aux = np.array([])
                    d.append(np.append(aux, [dis_mat[i,:,2] , dis_vet[i,:]]))
                    f.append(forces[i, 0 , :])
            
                                
                f = np.reshape(f,[np.size(f,0)*3])           # Vector of forces applied to the ion
                d = np.array(d)
            
                self.x_sample     =  d[0 : N_train*3, :]
                self.y_sample     =  f[0 : N_train*3] 
                #self.x_test       =  d[N_train*3  : N_train*3 + N_test*3, :]
                #self.y_test       =  f[N_train*3  : N_train*3 + N_test*3] 
                self.x_test       =  d[:, :]
                self.y_test       =  f
                self.topol        = topology
                self.weights      = weight/np.std(f)
                self.data_type    = "force"
                self.borders      = borders
                self.nfunctions   = nfunctions
                self.linparams    = linparams
                self.num_linpar   = num_linpar
                self.nlinparams   = nlinparams
                self.num_nlinpar  = num_nlinpar
                self.q_value      = q_value
                self.name         = name
            def eval_force(self,flag):
                """
                    Function which takes as input the data of our system (output of input_setup()) 
                    and returns the estimated values of the forces of each frame as Q, C6, C12
                """
                if self.data_type == "force":
                    flags = ["test", "train"]
                    if flag not in flags:
                        print("choose a proper flag between {}, {}".format(flags, flag))
                    
                    if flag == "test":
                        x = self.x_test
                    else:
                        x = self.x_sample

                    N_iter = np.size(self.x_sample, 1)/2
                    N_iter = int(N_iter)

                    dc  = x[:, 0 : N_iter ]
                    dt  = x[:, N_iter : N_iter*2]

                    # Charge Contribution
                    n_q = -1
                    q   = n_q*k_c*self.topol[0,1:]*dc[:,1:]*(np.power(dt[:,1:], (n_q - 2)))
                    q   = np.sum(q,1)

                    # C6 Contribution
                    n_6 = -6
                    c6  = n_6*np.multiply(dc[:,1:],(np.power(dt[:,1:], (n_6 - 2))))
                    c6  = c6* self.topol[1,1:]
                    c6  = np.sum(c6,1)

                    # C12 Contribution
                    n_12 = -12
                    c12  = n_12*np.multiply(dc[:,1:],np.power(dt[:,1:], (n_12 - 2)))
                    c12  = c12* self.topol[2,1:]
                    c12  = np.sum(c12,1)

                    if flag == "test":
                        self.q_test   = q
                        self.c6_test  = c6
                        self.c12_test = c12
                        return self.q_test, self.c6_test, self.c12_test
                    else:
                        self.q   = q
                        self.c6  = c6
                        self.c12  = c12
                        return self.q, self.c6, self.c12
                
        return data(topology, forces, dis_vet, dis_mat, N_train, N_test, borders, nfunctions, num_nlinpar, nlinparams, linparams, num_linpar, q_value ,name)

def training_set_MO(*args):
    """
        Function that takes as input all the input files and creates a unique training_set
    """
    
    class training_set():
        """
            args  = [Energy 128, Energy 32, ... Forces 128, Forces 32, ...]
        """
        def __init__(self, *args):
            """
                Initialization, system sensitive
            """
            print("Beware of the right order: e128, e32, f128, f32 ")
                        
            self.e128 = args[0]
            self.e32  = args[1]
            self.f128 = args[2]
            self.f32  = args[3]
            
        def __getitem__(self, i):
            """
                Method necessary to iterate through the class
            """
            if i == 0:
                return self.e128
            if i == 1:
                return self.e32
            if i == 2:
                return self.f128
            if i == 3:
                return self.f32
            else:
                print("Index not present in the training set {}".format(i))
                return -1

        def __len__(self):
            """
                Method necessary to evaluate the number of element of the class
            """
            return len(args)    
        
        def print(self):
            """
                Method which prints the data structures
            """
            msg = "x_sample, y_sample, x_test, y_test, topol, weights, data_type, borders, nfunctions, linparams, num_linpar, nlinparams, num_nlinpar "
            return("All data structure contains: " + msg)
        
        def model_descriptor(self):
            """
                Method which collects the non scaled descriptor of our system
            """
            self.q   =  np.hstack([self.e128.q,   self.e32.q,   self.f128.q,   self.f32.q])
            self.c6  =  np.hstack([self.e128.c6,  self.e32.c6,  self.f128.c6,  self.f32.c6])
            self.c12 =  np.hstack([self.e128.c12, self.e32.c12, self.f128.c12, self.f32.c12])
            self.borders     = self.e128.borders
            self.nfunctions  = self.e128.nfunctions
            self.q_value     = self.e128.q_value
        
            
    return training_set(*args)


def training_set_SO(*args):
    """
        Function that takes as input all the input files and creates a unique training_set
    """
    
    class training_set():
        """
            args  = [Energy 128, Energy 32, ... Forces 128, Forces 32, ...]
        """
        def __init__(self, *args):
            """
                Initialization, system sensitive
            """
            #print("Beware of the right order: e128, e32, f128, f32 ")
            if len(args) == 1:
                if args[0].name == "e128":
                    self.e128 = args[0]
                    self.name = 'e128'
                elif args[0].name == 'f128':
                    self.f128 = args[0]
                    self.name = 'f128'
                elif args[0].name == 'e32':
                    self.e32 = args[0]
                    self.name = 'e32'
                elif args[0].name == 'f32':
                    self.f32 = args[0]
                    self.name = 'f32'
            
        def __getitem__(self, i):
            """
                Method necessary to iterate through the class
            """
            if i == 0:
                if self.name == 'e128':
                    return self.e128
                elif self.name == 'f128':
                    return self.f128
                elif self.name == 'f32':
                    return self.f32
                elif self.name == 'e32':
                    return self.e32
                else:
                    return "Please choose an appropriate attribute"
            else:
                print("Index not present in the training set {}".format(i))
                return -1

        def __len__(self):
            """
                Method necessary to evaluate the number of element of the class
            """
            return len(args)    
        
        def print(self):
            """
                Method which prints the data structures
            """
            msg = "x_sample, y_sample, x_test, y_test, topol, weights, data_type, borders, nfunctions, linparams, num_linpar, nlinparams, num_nlinpar, q_value, name"
            return("All data structure contains: " + msg)
        
        def model_descriptor(self):
            """
                Method which collects the non scaled descriptor of our system
            """
            if self.name == "e128":
                self.q   =  np.hstack([self.e128.q])
                self.c6  = -np.hstack([self.e128.c6])
                self.c12 =  np.hstack([self.e128.c12])
                self.borders     = self.e128.borders
                self.nfunctions  = self.e128.nfunctions
                self.q_value     = self.e128.q_value
                
            elif self.name == "f128":
                self.q   =  np.hstack([self.f128.q])
                self.c6  = -np.hstack([self.f128.c6])
                self.c12 =  np.hstack([self.f128.c12])
                self.borders     = self.f128.borders
                self.nfunctions  = self.f128.nfunctions
                self.q_value     = self.f128.q_value
            elif self.name == "f32":
                self.q   =  np.hstack([self.f32.q])
                self.c6  = -np.hstack([self.f32.c6])
                self.c12 =  np.hstack([self.f32.c12])
                self.borders     = self.f32.borders
                self.nfunctions  = self.f32.nfunctions
                self.q_value     = self.f32.q_value
            elif self.name == "e32":
                self.q   =  np.hstack([self.e32.q])
                self.c6  = -np.hstack([self.e32.c6])
                self.c12 =  np.hstack([self.e32.c12])
                self.borders     = self.e32.borders
                self.nfunctions  = self.e32.nfunctions
                self.q_value     = self.e32.q_value
        
            
    return training_set(*args)