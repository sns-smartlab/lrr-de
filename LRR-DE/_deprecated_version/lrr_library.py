import os
import numpy as np
import matplotlib.pyplot as plt
import math
from lrr_de_constant import *

   

class model():
    """
        Model class
        


    """
    
    def __init__(self, data, num_lin_params, lin_params, num_nlin_params,\
            nlin_params, border = [10**-15, 10**-1], hy_lambda = 0):
        self.data            = data
        self.num_lin_params  = num_lin_params    # Number of linear parameters of the model
        self.lin_params      = lin_params        # Linear parameters 
        self.num_nlin_params = num_nlin_params   # Number of non linear parameters of the model
        self.nlin_params     = nlin_params       # Non linear parameters  
        self.border          = border            # Domain for the regularization parameter
        self.hy_lambda       = hy_lambda
      

