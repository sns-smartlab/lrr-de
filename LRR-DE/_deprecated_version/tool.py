import numpy as np
from lrr_de_constant import *

def build_ysample(data_set):
    """
        Function which collects in one array all the y sample (QM data)
    """
    y_sample = np.array([])
    for index in range(len(data_set)):
        y_sample = np.hstack([y_sample, data_set[index].y_sample ])
    return  y_sample


def build_wmatrix(data_set):
    """
        Function which collects in one array all the weight and builds the weight matrix
    """ 
    w_vector = np.array([])
    
    for index in range(len(data_set)):
        weight   = data_set[index].weights
        #weight   = 1
        n        = np.size(data_set[index].y_sample,0)
        aux      = weight*np.ones([n])*(1/n)
        w_vector = np.hstack([w_vector, aux])
        
    w_vector = np.array(w_vector)
    w_matrix = np.diag(w_vector)
    return w_vector, w_matrix


def standardize_data(data):
    """
        Function which takes as input a data array and
        returns his mean, standard deviation and
        the data divided by his std
    """
    mean     = np.mean(data)
    sigma    = np.std(data)
    data_std = np.divide(data, sigma)
    return mean, sigma, data_std

 
   
def leverage(data_set):
    """
        pancakes
    """
    if data_set.nfunctions == 3:

        # Non scaled Descriptor
        q   = data_set.q
        c6  = data_set.c6
        c12 = data_set.c12

        # Scaled Descriptor
        mean_q,   sigma_q,   data_std_q   = standardize_data(q)
        mean_c6,  sigma_c6,  data_std_c6  = standardize_data(c6)
        mean_c12, sigma_c12, data_std_c12 = standardize_data(c12)

        # Weight matrix
        v, W = build_wmatrix(data_set)

        H = np.matrix([data_std_q, data_std_c12, data_std_c6]).T   # MxNfunctionts
        
    elif data_set.nfunctions == 2:
                    
        # Non scaled Descriptor
        c6  = data_set.c6
        c12 = data_set.c12

        # Scaled Descriptor
        mean_c6,  sigma_c6,  data_std_c6  = standardize_data(c6)
        mean_c12, sigma_c12, data_std_c12 = standardize_data(c12)

        sigma  = np.array([sigma_c12, sigma_c6])

        # QM Reference data
        y    = build_ysample(data_set)

        # Weight matrix
        v, W = build_wmatrix(data_set)

        H = np.matrix([data_std_c12, data_std_c6]).T   # MxNfunctionts
    
    n = np.size(data_std_c12,0)
    V    = np.tile(v,(np.size(H,1),1))
    
    H = np.dot(H,V)
    
    mean = np.mean(H)
    Mean = np.ones(np.shape(H))
    dev  = H - Mean
    num  = np.sum(np.power(dev,2),1)
    den  = np.sum(num)
    
    l = 1/n + num/den
    return l
    