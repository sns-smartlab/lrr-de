import numpy as np
from tool import *
import matplotlib.pyplot as plt



def testing_data(data_set, params, old_params, plot = 'n'):
    """
        pancakes
    """
    if data_set.nfunctions == 2:
            params = np.append([data_set.q_value], params)
    
    for i in np.arange(len(data_set)):
        
        q   = data_set[i].q_test
        c6  = data_set[i].c6_test
        c12 = data_set[i].c12_test
        
        # Scaled Descriptor
        mean_q,   sigma_q,   data_std_q   = standardize_data(q)
        mean_c6,  sigma_c6,  data_std_c6  = standardize_data(c6)
        mean_c12, sigma_c12, data_std_c12 = standardize_data(c12)
        
        H = np.matrix([q, c12, c6]).T   # MxNfunctionts
        
        y_ref  = data_set[i].y_test
        y_test = np.dot(H, params.T).reshape(np.shape(y_ref)).T
        y_test = np.array(y_test)

        mae_test = np.mean(np.abs(y_test - y_ref.T))
        mse_test = np.mean(np.power(y_test - y_ref, 2))
        
        print("SET: ", data_set[i].data_type, "# of atoms {}".format(np.shape(data_set[i].topol)[-1]))
        print('MSE (test) = {}'.format(mse_test))
        print('MAE (test) = {}'.format(mae_test))
                
        q_old       = old_params[0]
        sigma_old   = old_params[1]
        epsilon_old = old_params[2]
        
        C12old = (4*(epsilon_old)*(sigma_old**12))**0.5
        C6old  = (4*(epsilon_old)*(sigma_old**6))**0.5
        
        c_old = np.array([q_old, C12old, C6old])
        
        y_test_old = np.dot(H, c_old.T).T
        y_test_old = np.array(y_test_old)
        
        mae_test_old = np.mean(np.abs(y_test_old.T - y_ref))
        mse_test_old = np.mean(np.power(y_test_old.T - y_ref, 2))
        
        #print('MSE (OLD) = {}\n'.format(mse_test_old))
        print('MAE (OLD) = {}\n'.format(mae_test_old))
        
        mse_old_test = np.mean(np.abs(y_test_old.T - y_test))
        print('MAE (OLD--TEST) = {}\n'.format(mse_old_test))
        
        if plot == 'y':
            
            y_ref.sort()
            y_test = np.sort(y_test,axis=0)
            y_test_old.sort(axis=0)
            plt.figure(dpi=100)
            plt.plot(y_test,label="test", marker='^')
            plt.plot(y_ref, label="ref", marker='^')
            plt.plot(y_test_old, label="old", marker='^')
            plt.legend()
            plt.show()